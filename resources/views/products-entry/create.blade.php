@extends('layouts.app')
@section('mainContent')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Product Entry</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('products.entry.index')}}">Product Entry</a></li>
                                <li class="breadcrumb-item active">create</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-default">
                            <div class="card-header">
                                <h3 class="card-title">Product Entry Info</h3>
                            </div>
                            <form action="{{url('products-entry/store')}}" class="p-2" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                    <label for="item_code" class="col-12 col-form-label">Item Code <span class="text-danger">*</span></label>
                                    <div class="col-lg-4">
                                        <div class="col-12">
                                            <input class="form-control custom-focus @error('item_code') is-invalid @enderror" placeholder="Product code" id="item_code" name="item_code" type="text" value="{{ old('item_code') }}">
                                        </div>
                                        @error('item_code')
                                        <span class="text-danger ml-3 mt-1">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-4">
                                        <div class="col-12">
                                            <a class="btn btn-info" id="add_product">Add</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-12">
                                        <table class="table table-bordered table-striped" id="product">
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Code</th>
                                                <th>Qty</th>
                                                <th>Price</th>
                                                <th>Total Price</th>
                                            </tr>
                                        </table>
                                        <div class="offset-8">
                                            <table class="table table-borderless">
                                                <tr>
                                                    <td class="text-bold float-right">Grand Total:</td>
                                                    <input type="hidden" name="grand_total" value="">
                                                    <td id="grand_total" class="text-bold"></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-11 col-1">
                                        <button type="submit" class="form-control btn-info">Product Entry</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('custom_js')
    <script>
        $(function () {
            $('.sev_check').click(function(e) {
                $('.sev_check').not(this).prop('checked', false);
            });
        });
        //remove item from product entry list
        $(function () {
            //$('.product').click(function(e) {
            $(document).on('click','.product',function(){
                var id = $(this).data('code');
              //  console.log(id);
                $('#'+id).remove();
                priceDataSync();
            });
        });

        // item wise total price change
        $(function () {
            $(document).on('change','.sales-item-input',function(){
                var id = $(this).data('code');
                var price = $('#item_price_'+id).val();
                var qty = $('#item_qty_'+id).val();
                var total_price = qty*price;
                console.log(total_price);
                $('#item_total_price_'+id).html(total_price);

                priceDataSync();
            });
        });

        /*load product using product code */
        $(document).on('click','#add_product',function(){
            var item_code = $('#item_code').val();
            //console.log(item_code);
            var url = '{{ url('/') }}' ;
            if(!item_code)
            {
                alert('Please give product code');
            }
            else{
                $.ajax({
                    type:'POST',
                    dataType:'json',
                    url: url + '/ajax/get-product',
                    data:{item_code:item_code},
                    success:function (data){
                        console.log(data);
                        if(data.length === 0)
                        {
                            alert('You do not have this item code');
                        }
                        else{
                            if(!$('.item_input_code').html())
                            {
                                $('#product').append('<tr id="'+data[0]["item_code"]+'"><td><i class="product fas fa-trash text-danger" aria-hidden="true" data-code="'+data[0]["item_code"]+'"></i></td>' +
                                    '<td>'+data[0]["item_name"]+'</td><td class="item_input_code">'+data[0]["item_code"]+'</td>' +
                                    '<td><input type="hidden" name="products['+data[0]["id"]+'][item_id]" value="'+data[0]["id"]+'">' +
                                    '<input type="hidden" name="products['+data[0]["id"]+'][item_name]" value="'+data[0]["item_name"]+'">' +
                                    '<input type="hidden" name="products['+data[0]["id"]+'][item_code]" value="'+data[0]["item_code"]+'">' +
                                    '<input id="item_qty_'+data[0]["item_code"]+'" class="form-control sales-item-input" type="number" name="products['+data[0]["id"]+'][qty]" data-code="'+data[0]["item_code"]+'" required></td>' +
                                    '<td><input id="item_price_'+data[0]["item_code"]+'" class="form-control sales-item-input" type="number" name="products['+data[0]["id"]+'][item_price]" data-code="'+data[0]["item_code"]+'" required></td>' +
                                    '<td class="total_price" id="item_total_price_'+data[0]["item_code"]+'"></td></tr>');
                                //priceDataSync();
                            }
                            else{
                                var codeArr=[];
                                $('.item_input_code').each(function() {
                                    codeArr.push($(this).html());

                                });
                                if(jQuery.inArray(item_code,codeArr) != -1)
                                {
                                    alert('You already added this product');
                                }
                                else{
                                    $('#product').append('<tr id="'+data[0]["item_code"]+'"><td><i class="product fas fa-trash text-danger" aria-hidden="true" data-code="'+data[0]["item_code"]+'"></i></td>' +
                                        '<td>'+data[0]["item_name"]+'</td>' +
                                        '<td class="item_input_code">'+data[0]["item_code"]+'</td>' +
                                        '<td><input type="hidden" name="products['+data[0]["id"]+'][item_id]" value="'+data[0]["id"]+'">' +
                                        '<input type="hidden" name="products['+data[0]["id"]+'][item_name]" value="'+data[0]["item_name"]+'">' +
                                        '<input type="hidden" name="products['+data[0]["id"]+'][item_code]" value="'+data[0]["item_code"]+'">' +
                                        '<input id="item_qty_'+data[0]["item_code"]+'" class="form-control sales-item-input" type="number" name="products['+data[0]["id"]+'][qty]" data-code="'+data[0]["item_code"]+'" required></td>' +
                                        '<td><input id="item_price_'+data[0]["item_code"]+'" class="form-control sales-item-input" type="number" name="products['+data[0]["id"]+'][item_price]" data-code="'+data[0]["item_code"]+'" required></td>' +
                                        '<td class="total_price" id="item_total_price_'+data[0]["item_code"]+'"></td></tr>');
                                }
                            }
                        }
                    }
                });
            }

        });

        //item amount check
        function priceDataSync()
        {
            // subtotal calculation
                var total_price_arr=[];
                var total_price=0;
                $('.total_price').each(function() { total_price_arr.push($(this).html()); });
                //console.log(total_price_arr);
                $.each(total_price_arr, function (key, val) {
                    total_price = total_price + parseInt(val);
                });
                //console.log(total_price);
                $('#grand_total').html(total_price);
                $("input[name='grand_total']").val(total_price);

        }
        //change amount load based on given amount
        $(function () {
            $(document).on('change','#given_amount',function(){
                var givenAmount = $(this).val();
                var grandTotal = $('#grandtotal').html();
               // console.log(givenAmount);
               // console.log(grandTotal);
                if(givenAmount && grandTotal)
                {
                    var change = givenAmount - grandTotal ;
                    $('#change_amount').val(change);
                }
                else{
                    $('#change_amount').val();
                }
            });
        });

    </script>
@endpush
