@extends('layouts.app')
@section('mainContent')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Product Entry</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('products.entry.index')}}">Product Entry</a></li>
                            <li class="breadcrumb-item active">Edit</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-default">
                            <div class="card-header">
                                <h3 class="card-title">Product Entry Info</h3>
                            </div>
                            <form action="{{url('products-entry/update')}}" class="p-2" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-12">
                                        <table class="table table-bordered table-striped" id="product">
                                            <thead>
                                            <tr>
                                                <th>SL</th>
                                                <th>Name</th>
                                                <th>Code</th>
                                                <th>Qty</th>
                                                <th>Price</th>
                                                <th>Total Price</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(isset($productEntryDetails))
                                                <?php
                                                $i = 1;
                                                $totalPrice = 0 ;
                                                ?>
                                                @foreach($productEntryDetails as $key => $v)
                                                    <tr id="gid{{ $v->id }}">
                                                        <td>{{ $i++ }}</td>
                                                        <td>{{ $v->item_name }}</td>
                                                        <td>{{ $v->item_code}}</td>
                                                        <input type="hidden" name="entry_id" value="{{$v->entry_id}}">
                                                        <input  type="hidden" name="products[{{$key}}][id]" value="{{$v->id}}">
                                                        <input type="hidden" name="products[{{$key}}][item_qty_old]" value="{{ $v->total_qty }}">
                                                        <input type="hidden" name="products[{{$key}}][item_price_old]" value="{{ $v->unit_price }}">
                                                        <input type="hidden" name="products[{{$key}}][sellable_qty_old]" value="{{ $v->sellable_qty }}">
                                                        <td><input type="number" class="form-control sales-item-input" id="item_qty_{{ $v->item_code}}" data-code="{{ $v->item_code}}"name="products[{{$key}}][item_qty]" value="{{ $v->total_qty }}"></td>
                                                        <td><input type="number" class="form-control sales-item-input" id="item_price_{{ $v->item_code}}" data-code="{{ $v->item_code}}" name="products[{{$key}}][item_price]" value="{{ $v->unit_price }}" {{($v->sellable_qty<$v->total_qty)?'readonly':''}}></td>
                                                        <td id="item_total_price_{{ $v->item_code}}" class="total_price">{{ $v->total_price }}</td>
                                                    </tr>
                                                    @php
                                                        $totalPrice = $totalPrice + $v->total_price ;
                                                    @endphp
                                                @endforeach
                                                <tr>
                                                    <td colspan="5" ><span class="float-right text-bold">Grand Total:</span></td>
                                                    <input type="hidden" name="grand_total" value="{{$totalPrice}}">
                                                    <td id="grand_total" class="text-bold">{{$totalPrice}}</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-10 col-2">
                                        <button type="submit" class="form-control btn-info">Update Product Entry</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('custom_js')
    <script>
        $(function () {
            $('.sev_check').click(function(e) {
                $('.sev_check').not(this).prop('checked', false);
            });
        });
        //remove item from product entry list


        // item wise total price change
        $(function () {
            $(document).on('change','.sales-item-input',function(){
                var id = $(this).data('code');
                var price = $('#item_price_'+id).val();
                var qty = $('#item_qty_'+id).val();
                var total_price = qty*price;
                console.log(total_price);
                $('#item_total_price_'+id).html(total_price);

                priceDataSync();
            });
        });

        //item amount check
        function priceDataSync()
        {
            // subtotal calculation
            var total_price_arr=[];
            var total_price=0;
            $('.total_price').each(function() { total_price_arr.push($(this).html()); });
            //console.log(total_price_arr);
            $.each(total_price_arr, function (key, val) {
                total_price = total_price + parseInt(val);
            });
            //console.log(total_price);
            $('#grand_total').html(total_price);
            $("input[name='grand_total']").val(total_price);

        }

        /*load product using product code */
        //item amount check

        //change amount load based on given amount
        $(function () {
            $(document).on('change','#given_amount',function(){
                var givenAmount = $(this).val();
                var grandTotal = $('#grandtotal').html();
                // console.log(givenAmount);
                // console.log(grandTotal);
                if(givenAmount && grandTotal)
                {
                    var change = givenAmount - grandTotal ;
                    $('#change_amount').val(change);
                }
                else{
                    $('#change_amount').val();
                }
            });
        });

    </script>
@endpush
