@extends('layouts.app')
@section('mainContent')
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card-body d-flex flex-row justify-content-start">
                            <h5>User Info</h5>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card-body flex-row text-right">
                            <a href="{{route('employee.create')}}" class="btn btn-sm btn-secondary my-2"
                               style="line-height: 1.5 !important;"><i class="fas fa-plus"></i> Add User</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-12">
                        <!-- card start -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">User List</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>User Name</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $k =>$user)
                                    <tr>
                                        <td>{{++$k}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>
                                            <a href="{{route('employee.edit',[$user->id])}}"
                                               class="btn btn-sm btn-warning my-2" title="Edit">
                                                <i class="fas fa-edit" aria-hidden="true"></i>
                                            </a>
                                            <a href="javascript:void(0)"
                                               onclick="deleteConfirm({{ $user->id }})"
                                               class="btn btn-sm btn-danger my-2" title="Delete">
                                                <i class="fas fa-trash" aria-hidden="true"></i>
                                            </a>
                                            <a href="#" data-toggle="modal" data-target="#changePasswordModal{{$user->id}}"
                                               class="btn btn-sm btn-success my-2" title="Password Change">
                                                <i class="fas fa-key" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div><!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Change Password Modal -->
    @foreach($users as $k =>$user)
        <div class="modal fade" id="changePasswordModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{route('employee.change.password')}}">
                            @csrf
                            <input type="hidden" name="user_id" value="{{$user->id}}">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <label for="old_password">Old Password<span class="text-danger">*</span></label>
                                        <input type="password" class="form-control" name="old_password" placeholder="Enter Old Password" max="50" required>
                                        <span class="text-center text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <label for="new_password">New Password<span class="text-danger">*</span></label>
                                        <input type="password" class="form-control" name="new_password" placeholder="Enter New Password" max="50" required>
                                        <span class="text-center text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12">
                                        <label for="confirm_password">Confirm Password<span class="text-danger">*</span></label>
                                        <input type="password" class="form-control" name="confirm_password" placeholder="Enter Confirm Password" max="50" required>
                                        <span class="text-center text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-info btn-custom">Change</button>
                            <button type="button" class="btn btn-secondary btn-custom" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
@push('custom_js')
    <!-- Page specific script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                //"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        });
        <!-- delete alert-->
        function deleteConfirm(id)
        {
            var token = $("meta[name='csrf-token']").attr("content");
            if (confirm("Are you sure to delete this record!")) {
                $.ajax({
                    url: 'employee/delete/' + id,
                    type: 'get',
                    success: function (status)
                    {
                        if(status.status==1){
                            window.location.reload();
                        }
                    }
                })
            }
        }
    </script>
@endpush
