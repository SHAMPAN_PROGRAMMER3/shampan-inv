@extends('layouts.app')
@section('mainContent')
    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                <div class="row mt-2">
                    <div class="col-md-12">
                        <div class="card card-secondary">
                            <div class="card-header">
                                <h3 class="card-title">Edit Employee</h3>
                            </div>
                            <form action="{{ route('employee.update') }}" class="p-2" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="emp_id" value="{{$users->id}}">
                                <div class="form-group row">
                                    <label for="name" class="col-12 col-form-label">Employee Name <span class="text-danger">*</span></label>
                                    <div class="col-12">
                                        <input class="form-control custom-focus @error('name') is-invalid @enderror" placeholder="Employee Name" id="name" name="name" type="text" value="{{ $users->name }}" required>
                                    </div>
                                    @error('name')
                                    <span class="text-danger ml-3 mt-1">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-12 col-form-label">Email Address<span class="text-danger">*</span></label>
                                    <div class="col-12">
                                        <input class="form-control custom-focus @error('email') is-invalid @enderror" placeholder="Employee email" id="email" name="email" type="email" value="{{ $users->email }}" required>
                                    </div>
                                    @error('email')
                                    <span class="text-danger ml-3 mt-1">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <div class="col-12 mt-3 mb-2 text-right">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
