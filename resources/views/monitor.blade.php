<!DOCTYPE html>
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    
</head>
<style>
    .back{
        background-color: aliceblue;
        height: 100%;
    }
    .div-design{
        overflow: auto;
        border: 1px ridge #997676;
        width:100%;
        height: 400px;
    }
</style>
<body>
<div class="container-fluid back">
    <div class="container">
        <h1 class="text-center mb-3">Shampan IT Solutions</h1>
    </div>
    <div class="container mt-3">
        <h3 class="text-center">Shampan App Monitoring</h3>
        <div class="container">
            <div class="row ">
                <div class="col-sm-6 col-md-6 ">
                    <iframe class="div-design" src="{{url('iframe')}}"  target="_parent"></iframe>
                    
                </div>
                <div class="col-sm-6 col-md-6">
                    <object class="div-design" type="text/html" data="https://www.shampanit.com/"></object>
                </div>
                <div class="col-sm-6 col-md-6 ">
                    <object class="div-design" type="text/html" data="http://13.250.107.117/public/"></object>
                </div>
                <div class="col-sm-6 col-md-6 ">
                    <object class="div-design" type="text/html" data="http://www.shampan.org/"></object>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    setTimeout(function(){
        window.location.reload();
    },60000);
</script>
</body>
</html>