<style type="text/css">
    #client0rder{
        display: table;
        width: 100%;
        font-size: 11px;
        border-collapse: collapse;
    }
    #client0rder, td, th {
        border: 1px solid black;
        font-size: 11px;
    }
    .text-bold {
        font-weight: bold;
    }
    .center{
        text-align: center;
    }
    .colspan{
        text-align: right;
    }
    .padding{
        padding: 5px;
    }
    .padding-buttom{
        padding-bottom: 10px;
    }
</style>
<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-sm-6 col-6">
                                <div class="card-body d-flex flex-row justify-content-end">
                                    <h4 class="text-bold center">Product Entry Report</h4>
                                </div>
                            </div>
                        </div>
                        <p>Date: {{date("d M, Y",strtotime($requests['entry_start_date']))}} to {{date("d M, Y",strtotime($requests['entry_end_date']))}}</p>
                        @if(isset($productEntryList))
                        @foreach($productEntryList as $entryList)
                            <div class="card-body table-responsive">
                            <form method="post" action="{{ route('products.entry.report.index') }}" enctype="multipart/form-data">
                                @csrf
                                <table class="table table-bordered table-striped padding-buttom" id="client0rder">
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Product Name</th>
                                        <th>Entry Date</th>
                                        <th>Sellable Qty</th>
                                        <th>Total Qty</th>
                                        <th>Unit Price</th>
                                        <th>Total Price</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($entryList))
                                        @php
                                            $i=1;
                                            $total=0;
                                            $var=0;
                                        @endphp
                                        @foreach($entryList as $v)
                                            <tr id="gid{{ $v->id }}">
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $v->item_name }}</td>
                                                <td>{{date("d M, Y",strtotime($v->created_at))}} </td>
                                                <td>{{ $v->sellable_qty }}</td>
                                                <td>{{ $v->total_qty }}</td>
                                                <td>{{ $v->unit_price }}</td>
                                                <td>{{ $v->total_price }}</td>
                                                @php $var= $v->total_price@endphp
                                                @php $total = $total + $var; @endphp
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="6" class="text-bold colspan padding"> <span class="float-right">GRAND TOTAL:</span></td>
                                            <td class="text-bold">{{$total}}</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </form>
                        </div>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
