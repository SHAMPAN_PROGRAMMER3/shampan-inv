@extends('layouts.app')
@section('mainContent')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6 col-6">
                        <div class="card-body d-flex flex-row justify-content-end">
                            <h4 class="text-bold">Product Entry Report</h4>
                        </div>
                    </div>
                </div>
                <div class="card card-widget">
                    <form action="{{ route('products.entry.report.index') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="name" class="col-12 col-form-label">From Date</label>
                                    <div class="col-12">
                                        <input type="date" class="form-control order_date_class" placeholder="Order Date" name="entry_start_date" id="entry_start_date" value="{{(isset($requests['entry_start_date'])?$requests['entry_start_date']:'')}}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="name" class="col-12 col-form-label">To Date</label>
                                    <div class="col-12">
                                        <input type="date" class="form-control order_date_class" placeholder="Order Date" name="entry_end_date" id="entry_end_date" value="{{(isset($requests['entry_end_date'])?$requests['entry_end_date']:'')}}" required>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="col-12 col-xl-3 col-sm-6">
                                <div class="form-group">
                                    <label for="name" class="col-12 col-form-label">Products</label>
                                    <div class="col-12">
                                        <select  class="form-control" name="item_id[]" id="item_id" multiple="multiple" required>
                                            @if(isset($allProducts))
                                                @foreach($allProducts as $product)
                                                    <option value="{{$product->id}}" {{isset($requests['item_id'])?in_array($product->id,$requests['item_id'])?'selected':'':''}}>{{$product->item_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>--}}
                        </div>

                        <div class="row">
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <label class="col-12 col-form-label">Products</label>
                                    <select class="duallistbox form-control" multiple="multiple" name="item_id[]" required>
                                        @if(isset($allProducts))
                                            @foreach($allProducts as $product)
                                                <option value="{{$product->id}}" {{isset($requests['item_id'])?in_array($product->id,$requests['item_id'])?'selected':'':''}}>{{$product->item_name}}</option>
                                            @endforeach
                                        @endif
                                        {{--<option selected>Alabama</option>
                                        <option>Alaska</option>
                                        <option>California</option>
                                        <option>Delaware</option>
                                        <option>Tennessee</option>
                                        <option>Texas</option>
                                        <option>Washington</option>--}}
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>

                        <div class="form-group row">
                            <div class="col-12 mt-3 mb-2 text-right">
                                <a class="btn btn-secondary" href="{{route('products.entry.report.index')}}">Reset</a>
                                <button type="submit"  name="submit" value="pdf" class="btn btn-danger">PDF</button>
                                <button type="submit"  name="submit" value="filter" class="btn btn-info mr-2">Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @if(isset($requests))
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Product Entry List</h3>
                            </div>
                            @if(isset($productEntryList))
                            @foreach($productEntryList as $entryList)
                            <div class="card-body table-responsive">
                                <table class="table table-bordered table-striped" id="group">
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Product Name</th>
                                        <th>Entry Date</th>
                                        <th>Sellable Qty</th>
                                        <th>Total Qty</th>
                                        <th>Unit Price</th>
                                        <th>Total Price</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($entryList))
                                        @php
                                            $i=1;
                                            $total=0;
                                            $var=0;
                                        @endphp
                                        @foreach($entryList as $v)
                                            <tr id="gid{{ $v->id }}">
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $v->item_name }}</td>
                                                <td>{{date("d M, Y",strtotime($v->created_at))}} </td>
                                                <td>{{ $v->sellable_qty }}</td>
                                                <td>{{ $v->total_qty }}</td>
                                                <td>{{ $v->unit_price }}</td>
                                                <td>{{ $v->total_price }}</td>
                                                @php $var= $v->total_price@endphp
                                                @php $total = $total + $var; @endphp
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="6" class="text-bold colspan"> <span class="float-right">GRAND TOTAL:</span></td>
                                            <td class="text-bold">{{$total}}</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            @endif
        </section>
    </div>
@endsection
@push('custom_js')
    <script>
    $(function () {

        //Bootstrap Duallistbox
        $('.duallistbox').bootstrapDualListbox();

    });
    </script>
@endpush
