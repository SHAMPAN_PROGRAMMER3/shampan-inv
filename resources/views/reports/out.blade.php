@extends('layouts.app')
@section('mainContent')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6 col-6">
                        <div class="card-body d-flex flex-row justify-content-end">
                            <h4 class="text-bold">Out Report</h4>
                        </div>
                    </div>
                </div>
                <div class="card card-widget">
                    <form action="{{ route('out.report.index') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="name" class="col-12 col-form-label">From Date</label>
                                    <div class="col-12">
                                        <input type="date" class="form-control order_date_class" placeholder="Order Date" name="entry_start_date" id="entry_start_date" value="{{(isset($requests['entry_start_date'])?$requests['entry_start_date']:'')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="name" class="col-12 col-form-label">To Date</label>
                                    <div class="col-12">
                                        <input type="date" class="form-control order_date_class" placeholder="Order Date" name="entry_end_date" id="entry_end_date" value="{{(isset($requests['entry_end_date'])?$requests['entry_end_date']:'')}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12 mt-3 mb-2 text-right">
                                <a class="btn btn-secondary" href="{{route('out.report.index')}}">Reset</a>
                                <button type="submit"  name="submit" value="pdf" class="btn btn-danger">PDF</button>
                                <button type="submit"  name="submit" value="filter" class="btn btn-info mr-2">Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @if(isset($requests))
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Out List</h3>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered table-striped" id="group">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Serial No</th>
                                    <th>Out Date</th>
                                    <th>Out By</th>
                                    <th>Total Product</th>
                                    <th>Total Price</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($productOutList))
                                    @php
                                        $i=1;
                                        $total=0;
                                        $var=0;
                                    @endphp
                                    @foreach($productOutList as $v)
                                        <tr id="gid{{ $v->id }}">
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $v->serial_no }}</td>
                                            <td>{{ $v->out_date}}</td>
                                            <td>{{ $v->emp_name }}</td>
                                            <td>{{ $v->product_type_qty }}</td>
                                            <td>{{ $v->total_price }}</td>
                                            @php $var= $v->total_price@endphp
                                            @php $total = $total + $var; @endphp
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="5" class="text-bold colspan"> <span class="float-right">GRAND TOTAL:</span></td>
                                        <td class="text-bold">{{$total}}</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </section>
    </div>
@endsection
