<style type="text/css">
    #client0rder{
        display: table;
        width: 100%;
        font-size: 11px;
        border-collapse: collapse;
    }
    #client0rder, td, th {
        border: 1px solid black;
        font-size: 11px;
    }
    .text-bold {
        font-weight: bold;
    }
    .center{
    text-align: center;
    }
    .colspan{
        text-align: right;
    }
</style>
<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="row">
                            <div class="col-sm-6 col-6">
                                <div class="card-body d-flex flex-row justify-content-end">
                                    <h4 class="text-bold center">Product Entry Report</h4>
                                </div>
                            </div>
                        </div>
                        <div class="card-body table-responsive">
                            <form method="post" action="{{ route('entry.report.index') }}" enctype="multipart/form-data">
                                @csrf
                                <table class="table table-bordered table-striped" id="client0rder">
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Serial No</th>
                                        <th>Entry Date</th>
                                        <th>Entry By</th>
                                        <th>Total Product</th>
                                        <th>Total Price</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($productEntryList))
                                        @php
                                            $i=1;
                                            $total=0;
                                            $var=0;
                                        @endphp
                                        @foreach($productEntryList as $v)
                                            <tr id="gid{{ $v->id }}">
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $v->serial_no }}</td>
                                                <td>{{ $v->entry_date}}</td>
                                                <td>{{ $v->emp_name }}</td>
                                                <td>{{ $v->product_type_qty }}</td>
                                                <td>{{ $v->total_price }}</td>
                                                @php $var= $v->total_price@endphp
                                                @php $total = $total + $var; @endphp
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="5" class="text-bold colspan"> <span class="float-right">GRAND TOTAL:</span></td>
                                            <td class="text-bold">{{$total}}</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
