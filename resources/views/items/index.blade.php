@extends('layouts.app')
@section('mainContent')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6 col-6">
                        <div class="card-body d-flex flex-row justify-content-end">
                            <h4 class="text-bold">Item List</h4>
                        </div>
                    </div>
                    <div class="col-sm-6 col-6">
                        <div class="card-body flex-row text-right">
                            <a href="{{route('items.create')}}" class="btn btn-sm btn-secondary my-2" title="Add New Item" style="line-height: 1.5 !important;">
                                <i class="fas fa-plus"></i>Add Item</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <form method="post" action="{{route('items.index')}}">
                                    @csrf
                                    <div class="form-group row">
                                        <div class="col-12 col-xl-4 col-sm-4">
                                            <label for="item_id" class="col-form-label">Item Code</label>
                                            <div class="col-12">
                                               @if(isset($requests['item_code']))
                                                    <input type="text" class="form-control" name="item_code" style="width: 100%;" value="{{$requests['item_code']}}">
                                                @else
                                                    <input type="text" class="form-control" name="item_code" style="width: 100%;" placeholder="Enter Item Code">
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12 mt-3 mb-2 text-right">
                                            <a class="btn btn-secondary" href="{{route('items.index')}}">Reset</a>
                                            <button type="submit" class="btn btn-info">Filter</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Item List</h3>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered table-striped" id="example1">
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Item Name</th>
                                        <th>Code</th>
                                        <th>Alert Qty</th>
                                        <th>Stock</th>
                                        <th>Amount</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($items))
                                       <?php $i = 1 ?>
                                        @foreach($items as $v)
                                        <tr id="gid{{ $v->id }}" class="{{(isset($v->alert_qty) && isset($v->total_qty))?($v->total_qty<=$v->alert_qty)?'text-danger text-bold':'':''}}">
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $v->item_name }}</td>
                                            <td>{{ $v->item_code }}</td>
                                            <td>{{ $v->alert_qty }}</td>
                                            <td>{{ $v->total_qty }}</td>
                                            <td>{{ $v->total_price }}</td>
                                            <td>
                                                @can('products.edit')
                                                <a href="{{route('items.edit',[$v->id])}}" class="btn btn-sm btn-warning my-2" title="Edit">
                                                    <i class="fas fa-edit" aria-hidden="true"></i>
                                                </a>
                                                @endcan
                                                    @can('products.delete')
                                                <a href="javascript:void(0)" onclick="deleteItem({{ $v->id }})" class="btn btn-sm btn-danger my-2" title="Delete">
                                                    <i class="fas fa-trash" aria-hidden="true"></i>
                                                </a>
                                                    @endcan
                                            </td>
                                        </tr>
                                       @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('custom_js')
    <!-- Page specific script -->
    <script>
        $(function () {
            $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                //"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        });
        <!-- delete alert-->


        function deleteItem(id)
        {
            var token = $("meta[name='csrf-token']").attr("content");
            if(confirm("Do you really want to delete this record"))
            {
                $.ajax({
                    url: 'items/delete/' + id,
                    type: 'get',
                    success: function (status)
                    {
                        if(status.status==1){
                            window.location.reload();
                        }
                    }
                })
            }
        }
    </script>
@endpush

