@extends('layouts.app')
@section('mainContent')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit Item</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('items.index')}}">Item</a></li>
                            <li class="breadcrumb-item active">Edit</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-default">
                            <div class="card-header">
                                <h3 class="card-title">Item Info</h3>
                            </div>
                            <form action="{{ route('items.update') }}" class="p-2" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="item_id" value="{{$item->id}}">
                                <div class="form-group row">
                                    <label for="item_name" class="col-12 col-form-label">Item Name <span class="text-danger">*</span></label>
                                    <div class="col-12">
                                        <input class="form-control custom-focus @error('item_name') is-invalid @enderror" placeholder="Enter Item Name" id="item_name" name="item_name" type="text" value="{{$item->item_name}}" required>
                                    </div>
                                    @error('item_name')
                                    <span class="text-danger ml-3 mt-1">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <label for="alert_qty" class="col-12 col-form-label">Alert Qty</label>
                                    <div class="col-12">
                                        <input class="form-control custom-focus @error('alert_qty') is-invalid @enderror" placeholder="Enter Alert Qty" id="alert_qty" name="alert_qty" type="text" value="{{$item->alert_qty}}">
                                    </div>
                                    @error('alert_qty')
                                    <span class="text-danger ml-3 mt-1">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <label for="item_code" class="col-12 col-form-label">Item Code <span class="text-danger">*</span></label>
                                    <div class="col-12">
                                        <input class="form-control custom-focus @error('item_code') is-invalid @enderror" placeholder="Item code" id="item_code" name="item_code" type="text" value="{{$item->item_code}}" required="required" readonly>
                                    </div>
                                    @error('item_code')
                                    <span class="text-danger ml-3 mt-1">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <label for="item_image" class="col-12 col-form-label">Image <span class="text-danger"></span></label>
                                    <div class="col-12">
                                        <input class="form-control custom-focus @error('item_image') is-invalid @enderror" placeholder="Image" id="item_image" name="item_image" type="file" value="{{$item->item_image }}">
                                        @if(isset($item->item_image))
                                            <br><span class="shampan-image"><img src="{{asset('/').'images/items/'.$item->item_image}}" ></span>
                                        @endif
                                    </div>
                                    @error('item_image')
                                    <span class="text-danger ml-3 mt-1">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <div class="col-12 mt-3 mb-2 text-right">
                                        <button type="submit" class="btn btn-info">update</button>
                                        <a href="{{ route('items.index') }}" class="btn btn-secondary">Go To Back</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('custom_js')
    <script>
        $(function () {
            $('.sev_check').click(function(e) {
                $('.sev_check').not(this).prop('checked', false);
            });
        });
    </script>
@endpush
