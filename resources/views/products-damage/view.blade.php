@extends('layouts.app')
@section('mainContent')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Product Damage</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('products.damage.index')}}">Product Entry</a></li>
                            <li class="breadcrumb-item active">create</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-default">
                            <div class="card-header">
                                <h3 class="card-title">Product Damage Details</h3>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <table class="table table-bordered table-striped" id="product">
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Code</th>
                                            <th>Product Damage Qty</th>
                                            {{--<th>Unit Price</th>--}}
                                            <th>Price</th>
                                        </tr>
                                        @foreach($productDamageDetails as $key=>$D)
                                            <tr>
                                                <td>{{++$key}}</td>
                                                <td>{{$D->item_name}}</td>
                                                <td>{{$D->item_code}}</td>
                                                <td>{{$D->product_qty}}</td>
                                                {{--<td>{{$D->unit_price}}</td>--}}
                                                <td>{{$D->total_price}}</td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="4" class="text-bold colspan"> <span class="float-right">GRAND TOTAL:</span></td>
                                            <td class="text-bold">{{$productDamageDetails[0]->grand_total}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('custom_js')
    <script>
        $(function () {
            $("#product").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#product_wrapper .col-md-6:eq(0)');
        });
        $(function () {
            $('.sev_check').click(function(e) {
                $('.sev_check').not(this).prop('checked', false);
            });
        });
    </script>
@endpush
