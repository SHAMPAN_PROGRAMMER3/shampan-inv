@extends('layouts.app')
@section('mainContent')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Product Damage</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('products.out.index')}}">Product Damage</a></li>
                            <li class="breadcrumb-item active">create</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-default">
                            <div class="card-header">
                                <h3 class="card-title">Product Damage Details</h3>
                            </div>
                            <form action="{{url('/products-damage/store')}}" class="p-2" method="post" enctype="multipart/form-data">
                            @csrf
                                <div class="form-group row">
                                    <div class="col-12">
                                        <table class="table table-bordered table-striped" id="product">
                                            <tr>
                                                <th>SL</th>
                                                <th>Name</th>
                                                <th>Code</th>
                                                <th>Out Qty</th>
                                                <th>Previous Return Qty</th>
                                                <th>Previous Damage Qty</th>
                                                <th>Damage Qty</th>
                                            </tr>
                                            @foreach($productOutDetails as $key=>$D)
                                                <tr>
                                                    <td>{{++$key}}</td>
                                                    <td>{{$D->item_name}}</td>
                                                    <td>{{$D->item_code}}</td>
                                                    <td id="total_qty_{{$D->product_id}}">{{$D->product_qty}}</td>
                                                    <td id="return_{{$D->product_id}}">{{isset($return[$D->product_id])?$return[$D->product_id]:''}}</td>
                                                    <td id="damage_{{$D->product_id}}">{{isset($damage[$D->product_id])?$damage[$D->product_id]:''}}</td>
                                                    
                                                    @if(isset($history[$D->id]))
                                                        <input type="hidden" name="products[{{$key}}][total_qty]" value="{{$D->product_qty}}">
                                                        <input type="hidden" name="products[{{$key}}][previous_return]" value="{{isset($return[$D->product_id])?$return[$D->product_id]:0}}">
                                                        <input type="hidden" name="products[{{$key}}][previous_damage]" value="{{isset($damage[$D->product_id])?$damage[$D->product_id]:0}}">

                                                        <td>
                                                        <input class="form-control custom-focus" id="item_id" name="products[{{$key}}][item_id]" type="hidden" value="{{$D->product_id}}">
                                                        <input class="form-control custom-focus" id="item_name" name="products[{{$key}}][item_name]" type="hidden" value="{{$D->item_name}}">
                                                        <input class="form-control custom-focus" id="item_code" name="products[{{$key}}][item_code]" type="hidden" value="{{$D->item_code}}">
                                                        <input type="hidden" name="products[{{$key}}][qty_price_history]" value="{{json_encode($history[$D->id])}}">
                                                        <input class="form-control custom-focus" id="out_id" name="products[{{$key}}][out_id]" type="hidden" value="{{$D->out_id}}">
                                                       <input class="form-control custom-focus damage_qty" placeholder="Enter Product Damage Qty" data-id="{{$D->product_id}}" id="damage_qty_{{$D->product_id}}" name="products[{{$key}}][damage_qty]" type="number">
                                                        </td>
                                                    @else
                                                        <td>
                                                            There is no quantity to damage
                                                        </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-10 col-2">
                                        <button type="submit" class="form-control btn-info">Entry Damage Products</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('custom_js')
    <script>
        $(function () {
            $("#product").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#product_wrapper .col-md-6:eq(0)');
        });
        $(function () {
            $(document).on('change','.damage_qty',function(){
                var value = $(this).val();
                var id = $(this).data('id');
                var totalQty = parseInt($('#total_qty_'+id).html());
                var returnQty = parseInt($('#return_'+id).html());
                var damageQty = parseInt($('#damage_'+id).html());
                if(isNaN(returnQty))
                {
                    returnQty = 0;
                }
                if(isNaN(damageQty))
                {
                    damageQty = 0;
                }
                if(value>(totalQty -returnQty -damageQty) || value<0)
                {
                    $('#damage_qty_'+id).val('');
                }
            });
        });
    </script>
@endpush
