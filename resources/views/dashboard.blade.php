
@extends('layouts.app')
@push('custom_css')
  <!-- fullCalendar -->
  <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/main.css') }}">
@endpush
@section('mainContent')
<div class="content-wrapper">
    <!-- Main content -->
    {{--<section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-info">
                        <div class="inner">
                            @if(!isset($data))
                               @if(!isset($totalEmployee))
                                <h3>{{0}}</h3>
                                @else
                                <h3>{{$totalEmployee}}</h3>
                                @endif
                                <p>Total Employee</p>
                            @else
                                <h3>Employee</h3>
                                @foreach($data["totalEmployee"] as $key => $value)
                                    <h5>{{$key.'-'}}
                                    @if($value == NULL)
                                        0
                                    @else
                                        {{' '.$value}}
                                    </h5>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-success">
                        <div class="inner">
                            @if(!isset($data))

                                @if(!isset($totalPresent))
                                <h3>{{0}}</h3>
                                @else
                                <h3>{{$totalPresent}}</h3>
                                @endif
                                <p>Present Employee</p>
                            @else
                                <h3>Present</h3>
                                @foreach($data["totalPresent"] as $key => $value)
                                    <h5>{{$key.'-'}}
                                    @if($value == NULL)
                                       0
                                    @else
                                        {{' '.$value}}
                                    </h5>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-warning">
                        <div class="inner">
                            @if(!isset($data))

                                @if(!isset($totalPresent))
                                <h3>{{0}}</h3>
                                @else
                                <h3>{{$totalEmployee - $totalPresent - $totalLeave}}</h3>
                                @endif
                                <p>Absent Employee</p>
                            @else
                                <h3>Absent</h3>
                                @foreach($data['totalAbsent'] as $key => $value)
                                    <h5>{{$key.'-'}}
                                    @if($value == NULL)
                                        0
                                    @else
                                        {{' '.$value}}
                                    </h5>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-danger">
                        <div class="inner">
                            @if(!isset($data))

                                @if(!isset($totalLeave))
                                <h3>{{0}}</h3>
                                @else
                                <h3>{{$totalLeave}}</h3>
                                @endif
                                <p>Leave Employee</p>
                            @else
                                <h3>Leave</h3>
                                @foreach($data["totalLeave"] as $key => $value)
                                    <h5>{{$key.'-'}}
                                    @if($value == NULL)
                                       0
                                    @else
                                        {{' '.$value}}
                                    </h5>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>--}}
    <!-- calender -->
        <!-- Main content -->
    {{--<section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="sticky-top mb-3">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Events</h4>
                            </div>
                            <div class="card-body">
                                <div id="external-events">
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Notices</h4>
                            </div>
                            <div class="card-body" style="max-height: 250px;overflow-y: scroll;">
                                @if(isset($notice[0]) && !empty($notice[0]))
                                @foreach( $notice  as $note)
                                <div id="external-notices">
                                        <b>{{ $note->notice_title}}</b>
                                        <p>{{ $note->notice}}</p>
                                    </div>
                                @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card card-primary">
                        <div class="card-body p-0">
                            <div id="calendar"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>--}}
</div>
@endsection
@push('custom_js')
  <!-- fullCalendar 2.2.5 -->
  <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
  <script src="{{ asset('plugins/fullcalendar/main.js') }}"></script>
  <!-- Page specific script -->
<script>
    $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
    });
    /*find event load data*/
    var url = '{{ url('/') }}';
    $.post({
        type:'POST',
        dataType:'json',
        url: url + '/ajax/deventload',
        success:function (data)
        {
         var data = data;
          eventLoad(data);
        }
    });

  function eventLoad(data) {

    /* initialize the external events
     -----------------------------------------------------------------*/
    function ini_events(ele) {
      ele.each(function () {

        // create an Event Object (https://fullcalendar.io/docs/event-object)
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        }

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject)

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex        : 1070,
          revert        : true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        })

      })
    }

    ini_events($('#external-events div.external-event'))

    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

    var Calendar = FullCalendar.Calendar;
    var Draggable = FullCalendar.Draggable;

    var containerEl = document.getElementById('external-events');
    var checkbox = document.getElementById('drop-remove');
    var calendarEl = document.getElementById('calendar');
    //  load data

    var eventall = data.data;
    var ty = data.y;
    var tm = data.m;
    var td = data.d;
    var th = data.h;
    var ti = data.i;
    var fEvents = [];
    var eList = [];
    $('#external-events').empty();
    for (let i in eventall)
      {


       let dateT = new Date(ty[eventall[i].id], tm[eventall[i].id], td[eventall[i].id],th[eventall[i].id], ti[eventall[i].id]);

          //let dateT = new Date(y, m, d, 10, 30);
          // console.log(dateT);

          let modulu = ((5+i) % 5);
          //console.log(modulu);
              if(modulu == 0)
              {
                btnK = 'bg-success';
                color = '#00a65a';
              }
              else if(modulu == 1)
              {
                btnK = 'bg-warning';
                color = '#f39c12';
              }
              else if(modulu == 2)
              {
                btnK = 'bg-info';
                color = '#00c0ef';
              }
              else if(modulu == 3)
              {
                btnK = 'bg-primary';
                color = '#3c8dbc';
              }
              else if(modulu == 4)
              {
                btnK = 'bg-danger';
                color = '#f56954';
              }
              let titleC = eventall[i].event_name+ ' at ' +eventall[i].event_timee +' in '+ eventall[i].event_place;
              fEvents.push(                {
                title          : titleC,
                start          : eventall[i].event_date,
                backgroundColor: color, //red
                borderColor    : color, //red
                allDay         : true
              },);



              let ldiv = '<div class="external-event '+btnK+'">'+eventall[i].event_name+' at '+eventall[i].event_date+' '+eventall[i].event_timee+' in '+eventall[i].event_place+'</div>';
              eList.push(ldiv);


      }

      $('#external-events').append(eList);

    // initialize the external events
    // -----------------------------------------------------------------

    new Draggable(containerEl, {
      itemSelector: '.external-event',
      eventData: function(eventEl) {
        return {
          title: eventEl.innerText,
          backgroundColor: window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
          borderColor: window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
          textColor: window.getComputedStyle( eventEl ,null).getPropertyValue('color'),
        };
      }
    });

    var calendar = new Calendar(calendarEl, {
      headerToolbar: {
        left  : 'prev,next today',
        center: 'title',
        right : 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      themeSystem: 'bootstrap',
      //Random default events

      events: fEvents,
      editable  : true,
      droppable : true, // this allows things to be dropped onto the calendar !!!
      drop      : function(info) {
        // is the "remove after drop" checkbox checked?
        if (checkbox.checked) {
          // if so, remove the element from the "Draggable Events" list
          info.draggedEl.parentNode.removeChild(info.draggedEl);
        }
      }
    });

    calendar.render();
    // $('#calendar').fullCalendar()

    /* ADDING EVENTS */
    var currColor = '#3c8dbc' //Red by default
    // Color chooser button
    $('#color-chooser > li > a').click(function (e) {
      e.preventDefault()
      // Save color
      currColor = $(this).css('color')
      // Add color effect to button
      $('#add-new-event').css({
        'background-color': currColor,
        'border-color'    : currColor
      })
    })
    $('#add-new-event').click(function (e) {
      e.preventDefault()
      // Get value and make sure it is not null
      var val = $('#new-event').val()
      if (val.length == 0) {
        return
      }

      // Create events
      var event = $('<div />')
      event.css({
        'background-color': currColor,
        'border-color'    : currColor,
        'color'           : '#fff'
      }).addClass('external-event')
      event.text(val)
      $('#external-events').prepend(event)

      // Add draggable funtionality
      ini_events(event)

      // Remove event from text input
      $('#new-event').val('')
    })
  }
</script>
@endpush




