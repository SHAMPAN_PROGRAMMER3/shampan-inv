@extends('layouts.app')
@section('mainContent')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card-body d-flex flex-row justify-content-start">
                        <h5>Employee List</h5>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card-body flex-row text-right">
                        <a href="{{--{{route('employees.create')}}--}}" class="btn btn-sm btn-secondary my-2" style="line-height: 1.5 !important;">Add New Employee</a>
                    </div>
                </div>
            </div>
            <!-- filter start -->
            {{--<div class="card card-widget">
                <form action="{{ route('admin.list.filter') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-sm-6 col-12 col-xl-3">
                            <div class="form-group">
                                <label for="company_name" class="col-12 col-form-label">Select Company</label>
                                <div class="col-12">
                                    @if(isset($group_id) && $group_id != null)
                                        <select class="form-control select2" id="group_id" name="group_id"
                                                style="width: 100%;">
                                            @foreach($group as $groupdata)
                                                @if($groupdata->id == $group_id)
                                                    <option value="{{ $groupdata->id }}"
                                                            selected>{{ $groupdata->group_name }}</option>
                                                @else
                                                    <option
                                                        value="{{ $groupdata->id }}">{{ $groupdata->group_name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    @else
                                        <select class="form-control select2" id="group_id" name="group_id"
                                                style="width: 100%;">
                                            <option value="">Select Company</option>
                                            @foreach($group as $groupdata)
                                                <option
                                                    value="{{ $groupdata->id }}">{{ $groupdata->group_name }}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-12 col-xl-3">
                            <div class="form-group">
                                <label for="name" class="col-12 col-form-label">Select Department</label>
                                <div class="col-12">
                                    @if(isset($dep_id) && $dep_id != null)
                                        <select class="form-control select2" id="dep_id" name="dep_id"
                                                style="width: 100%;">
                                            @foreach($department as $dept)
                                                @if($dept->id == $dep_id)
                                                    <option value="{{ $dept->id }}"
                                                            selected>{{ $dept->dep_name }}</option>
                                                @else
                                                    <option value="{{ $dept->id }}">{{ $dept->dep_name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    @else
                                        <select class="form-control select2" id="dep_id" name="dep_id"
                                                style="width: 100%;">
                                            <option value="">Select Department</option>
                                            @foreach($department as $dept)
                                                <option value="{{ $dept->id }}">{{ $dept->dep_name }}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-12 col-xl-3">
                            <div class="form-group">
                                <label for="name" class="col-12 col-form-label">Select Designation</label>
                                <div class="col-12">
                                    @if(isset($desi_id) && $desi_id != null)
                                        <select class="form-control select2" id="desi_id" name="desi_id"
                                                style="width: 100%;">
                                            @foreach($designation as $desi)
                                                @if($desi->id == $desi_id)
                                                    <option value="{{ $desi->id }}"
                                                            selected>{{ $desi->desi_name }}</option>
                                                @else
                                                    <option value="{{ $desi->id }}">{{ $desi->desi_name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    @else
                                        <select class="form-control select2" id="desi_id" name="desi_id"
                                                style="width: 100%;">
                                            <option value="">Select Designation</option>
                                            @foreach($designation as $desi)
                                                <option value="{{ $desi->id }}">{{ $desi->desi_name }}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-12 col-xl-3">
                            <div class="form-group">
                                <label for="name" class="col-12 col-form-label">Select Role</label>
                                <div class="col-12">
                                    @if(isset($role_id) && $role_id != null)
                                        <select class="form-control select2" id="role_id" name="role_id"
                                                style="width: 100%;">
                                            @foreach($adminRole as $role)
                                                @if($role->id == $role_id)
                                                    <option value="{{ $role->id }}"
                                                            selected>{{ $role->name }}</option>
                                                @else
                                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    @else
                                        <select class="form-control select2" id="role_id" name="role_id"
                                                style="width: 100%;">
                                            <option value="">Select Role</option>
                                            @foreach($adminRole as $role)
                                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 mt-3 mb-2 text-right">
                            <a href="{{route('admin.list')}}" class="btn btn-secondary my-2" style="line-height: 1.5 !important;"> Reset</a>
                            <button type="submit" class="btn btn-info"><i class="fas fa-filter"></i> Filter</button>
                        </div>
                    </div>
                </form>
            </div>
            --}}
            <!-- Filter ended -->
            <div class="row">
                <div class="col-12 col-md-12">
                    <!-- card start -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Employee List</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="adminList" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Admin Name</th>
                                    <th>Email</th>
                                    {{--<th>E-code</th>
                                    <th>Company</th>
                                    <th>Department</th>
                                    <th>Designation</th>--}}
                                    <th>Role type</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--@php $i = ($paginateData->currentpage() - 1) * $paginateData->perpage() + 1; @endphp--}}
                                @php $i = 1; @endphp
                                @if(isset($paginateData))
                                    @foreach($paginateData as $k => $v)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $v->name }}</td>
                                            <td>{{ $v->email }}</td>
                                            {{--<td>{{ $v->emp_code }}</td>
                                            <td>{{ $v->company_name }}</td>
                                            <td>{{ $v->dep_name }}</td>
                                            <td>{{ $v->desi_name }}</td>--}}
                                            <td>{{ $v->role_name }}</td>
                                            <td>
                                                <a href="{{URL::to('admin/edit/'.$v->id)}}" class="btn btn-sm btn-warning my-2" title="Edit">
                                                    <i class="fas fa-edit" aria-hidden="true"></i>
                                                </a>
                                                <a href="{{URL::to('roleAssign/'.$v->id)}}" class="btn btn-sm btn-success my-2" title="Role Assign">
                                                    <i class="fas fa-tasks" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <h1>No Data Found!</h1>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        <!-- /.row -->
        </div>
    </section>
</div>
@endsection
@push('custom_js')
    <!-- Page specific script -->
    <script>
        $(function () {
            $("#adminList").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        });
    </script>
@endpush
