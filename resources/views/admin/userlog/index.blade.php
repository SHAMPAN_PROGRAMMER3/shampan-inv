@extends('layouts.app')
@section('mainContent')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card-body d-flex flex-row justify-content-start">
                            <h5>User Log Report</h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <form method="post" action="{{route('admin.logreport')}}">
                                    @csrf
                                    <div class="form-group row">
                                        <div class="col-2">
                                            <label for="created_at" class="col-form-label">Log Date From</label>
                                            <input class="form-control custom-focus log_date_class" placeholder="Log Date" id="log_date_from" name="log_date_from" type="date" value="{{isset($request['log_date_from'])?$request['log_date_from']:''}}">
                                        </div>
                                        <div class="col-2">
                                            <label for="created_at" class="col-form-label">Log Date To</label>
                                            <input class="form-control custom-focus log_date_class" placeholder="Log Date" id="log_date_to" name="log_date_to" type="date" value="{{isset($request['log_date_to'])?$request['log_date_to']:''}}">
                                        </div>
                                        <div class="col-2">
                                            <label for="group_name" class="col-form-label">Company</label>
                                                @if(isset($group_id) && $group_id != null)
                                                    <select class="form-control select2" id="group_id" name="group_id"
                                                            style="width: 100%;">
                                                        @foreach($group as $allFilter)
                                                            @if($allFilter->id == $group_id)
                                                                <option value="{{ $allFilter->id }}"
                                                                        selected>{{ $allFilter->group_name }}</option>
                                                            @else
                                                                <option
                                                                    value="{{ $allFilter->id }}">{{ $allFilter->group_name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                @else
                                                <select class="form-control select2" id="group_id" name="group_id"
                                                        style="width: 100%;">
                                                    <option value="">Select Company</option>
                                                    @foreach($group as $allFilter)
                                                        <option
                                                            value="{{ $allFilter->id }}">{{ $allFilter->group_name }}</option>
                                                    @endforeach
                                                </select>
                                                @endif
                                            {{--<select class="form-control select2" id="companyId" name="group_name" style="width:100%;">
                                                <option value="">Select Company</option>
                                                @if(isset($company))
                                                    @foreach($company as $allFilter)
                                                        <option value="{{$allFilter->group_name}}" {{isset($request['group_name'])?($request['group_name'] == $allFilter->group_name)?'selected':'':''}}>{{$allFilter->group_name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>--}}
                                        </div>
                                        <div class="col-2">
                                            <label for="dep_name" class="col-form-label">Department</label>
                                            @if(isset($dep_id) && $dep_id != null)
                                                <select class="form-control select2" id="dep_id" name="dep_id"
                                                        style="width: 100%;">
                                                    @foreach($department as $dept)
                                                        @if($dept->id == $dep_id)
                                                            <option value="{{ $dept->id }}"
                                                                    selected>{{ $dept->dep_name }}</option>
                                                        @else
                                                            <option value="{{ $dept->id }}">{{ $dept->dep_name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            @else
                                                <select class="form-control select2" id="dep_id" name="dep_id"
                                                        style="width: 100%;">
                                                    <option value="">Select Department</option>
                                                    @foreach($department as $dept)
                                                        <option value="{{ $dept->id }}">{{ $dept->dep_name }}</option>
                                                    @endforeach
                                                </select>
                                            @endif


                                            {{--<select class="form-control select2" id="dep_name" name="dep_name" style="width:100%;">
                                                <option value="">Select Department</option>
                                                 @if(isset($department))
                                                     @foreach($department as $allFilter)
                                                         <option value="{{$allFilter->dep_name}}" {{isset($request['dep_name'])?($request['dep_name'] == $allFilter->dep_name)?'selected':'':''}}>{{$allFilter->dep_name}}</option>
                                                     @endforeach
                                                 @endif
                                            </select>--}}
                                        </div>
                                        <div class="col-2">
                                            <label for="desi_name" class="col-form-label">Designation</label>
                                            @if(isset($desi_id) && $desi_id != null)
                                                <select class="form-control select2" id="desi_id" name="desi_id"
                                                        style="width: 100%;">
                                                    @foreach($designation as $desi)
                                                        @if($desi->id == $desi_id)
                                                            <option value="{{ $desi->id }}"
                                                                    selected>{{ $desi->desi_name }}</option>
                                                        @else
                                                            <option value="{{ $desi->id }}">{{ $desi->desi_name }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            @else
                                                <select class="form-control select2" id="desi_id" name="desi_id"
                                                        style="width: 100%;">
                                                    <option value="">Select Designation</option>
                                                    @foreach($designation as $desi)
                                                        <option value="{{ $desi->id }}">{{ $desi->desi_name }}</option>
                                                    @endforeach
                                                </select>
                                            @endif
                                            {{--<select class="form-control select2" id="desi_name" name="desi_name" style="width:100%;">
                                                <option value="">Select Designation</option>
                                                @if(isset($designation))
                                                    @foreach($designation as $allFilter)
                                                        <option value="{{$allFilter->desi_name}}" {{isset($request['desi_name'])?($request['desi_name'] == $allFilter->desi_name)?'selected':'':''}}>{{$allFilter->desi_name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>--}}
                                        </div>
                                        <div class="col-2">
                                            <label for="emp_name" class="col-form-label">Employee</label>
                                            {{--<select class="form-control select2" id="emp_name" name="emp_name" style="width:100%;">
                                                <option value="">Select Employee</option>
                                                @if(isset($allEmployee))
                                                    @foreach($allEmployee as $allFilter)
                                                        <option value="{{$allFilter->id}}" {{isset($request['emp_name'])?($request['emp_name'] == $allFilter->id)?'selected':'':''}}>{{$allFilter->emp_name.' '.$allFilter->middle_name.' '.$allFilter->last_name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>--}}

                                            @if(isset($emp_id) && $emp_id != null)
                                                <select class="form-control select2" id="emp_id" name="emp_id"
                                                        style="width: 100%;">
                                                    @foreach($allEmployee as $emp)
                                                        @if($emp->id == $emp_id)
                                                            <option value="{{ $emp->id }}"
                                                                    selected>{{$emp->emp_name.' '.$emp->middle_name.' '.$emp->last_name}}</option>
                                                        @else
                                                            <option value="{{ $emp->id }}">{{$emp->emp_name.' '.$emp->middle_name.' '.$emp->last_name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            @else
                                                <select class="form-control select2" id="emp_id" name="emp_id"
                                                        style="width: 100%;">
                                                    <option value="">Select Designation</option>
                                                    @foreach($allEmployee as $emp)
                                                        <option value="{{ $emp->id }}">{{$emp->emp_name.' '.$emp->middle_name.' '.$emp->last_name}}</option>
                                                    @endforeach
                                                </select>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-12 mt-3 mb-2 text-right">
                                            <a class="btn btn-secondary" href="{{route('admin.logreport')}}">Reset</a>
                                            <button type="submit" class="btn btn-info">Filter</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">User Log Report</h3>
                            </div>
                            <div class="card-body table-responsive">
                                <table class="table table-bordered table-striped" id="logreports">
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Company</th>
                                        <th>Department</th>
                                        <th>Designation</th>
                                        <th>Employee Name</th>
                                        <th>Event </th>
                                        <th>Log Details</th>
                                        <th>Date </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($report))
                                        <?php $i = 1 ?>
                                        @foreach($report as $reportlist)
                                            <tr id="gid{{ $reportlist->id }}">
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $reportlist->group_name }}</td>
                                                <td>{{ $reportlist->dep_name }}</td>
                                                <td>{{ $reportlist->desi_name }}</td>
                                                <td>{{ $reportlist->emp_name.' '.$reportlist->middle_name.' '.$reportlist->last_name}}</td>
                                                <td>{{ $reportlist->event }}</td>
                                                <td>{{ $reportlist->details }}</td>
                                                <td>{{ $reportlist->created_at }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td><h4>No Data Found!</h4></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('custom_js')
    <!-- Page specific script -->
    <script>
        $(function () {
            $("#logreports").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#logreports_wrapper .col-md-6:eq(0)');
        });
        //
       /* $(document).on('change','.log_date_class',function(e) {
            var logStartDate = $('#log_date_from').val();
            var logEndDate = $('#log_date_to').val();

            var url = '{{ url('/') }}';
            if(logStartDate != '' && logEndDate != '')
            {
                $.ajax({
                    url: url + '/get-log-data',
                    method: 'post',
                    data:{
                        'logStartDate':logStartDate,
                        'logEndDate':logEndDate,
                    },
                    success: function (data) {
                        var companyData = data;
                        if(companyData){
                            /!*$('#companyId').empty();
                            $('#companyId').append('<option value="">Select Company No</option>');*!/
                            companyData.forEach(function (companyData) {
                                $('#companyId').append('<option value="'+companyData.id+'">' +
                                    companyData.group_name+
                                    '</option>');
                            })
                        }
                        else{
                            $('#companyId').empty();
                            $('#companyId').append('<option value="">Select Company</option>');
                        }
                    }
                });
            }
            else{
                $('#companyId').empty();
                $('#companyId').append('<option value="">Select Company</option>');
            }
        });*/
    </script>

@endpush

