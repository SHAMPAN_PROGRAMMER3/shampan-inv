@extends('layouts.app')
@section('mainContent')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6 col-6">
                        <div class="card-body d-flex flex-row justify-content-end">
                            <h4 class="text-bold">Product Return List</h4>
                        </div>
                    </div>
                    {{--<div class="col-sm-6 col-6">
                        <div class="card-body flex-row text-right">
                            <a href="{{route('products.entry.create')}}" class="btn btn-sm btn-secondary my-2" title="Add New Product Entry" style="line-height: 1.5 !important;">
                                <i class="fas fa-plus"></i> Add Product Entry</a>
                        </div>
                    </div>--}}
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Product Return List</h3>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered table-striped" id="productEntry">
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Serial No</th>
                                        <th>Return Date</th>
                                        <th>Return By</th>
                                        <th>Total Product</th>
                                        <th>Total Price</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($productReturnList))
                                        <?php $i = 1 ?>
                                        @foreach($productReturnList as $v)
                                            <tr id="gid{{ $v->id }}">
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $v->serial_no }}</td>
                                                <td>{{ $v->return_date}}</td>
                                                <td>{{ $v->emp_name }}</td>
                                                <td>{{ $v->product_type_qty }}</td>
                                                <td>{{ $v->total_price }}</td>
                                                <td>
                                                    <a href="{{URL::to('products-return/view/'.$v->id)}}" class="btn btn-sm btn-info my-2" title="View">
                                                        <i class="fas fa-eye" aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                                {{--<td>
                                                    <a href="{{URL::to('products-entry/view/'.$v->id)}}" class="btn btn-sm btn-info my-2" title="View">
                                                        <i class="fas fa-eye" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="{{URL::to('products-entry/edit/'.$v->id)}}" class="btn btn-sm btn-warning my-2" title="Edit">
                                                        <i class="fas fa-edit" aria-hidden="true"></i>
                                                    </a>
                                                </td>--}}
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('custom_js')
    <!-- Page specific script -->
    <script>
        $(function () {
            $("#productEntry").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                //"buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#productEntry_wrapper .col-md-6:eq(0)');
        });
    </script>
@endpush
