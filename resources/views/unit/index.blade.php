@extends('layouts.app')
@section('mainContent')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card-body d-flex flex-row justify-content-start">
                            <h5>Unit Info</h5>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card-body flex-row text-right">
                            <a href="{{route('unit.index')}}" class="btn btn-sm btn-secondary my-2"
                               style="line-height: 1.5 !important;">Reset</a>
                            <a href="{{route('unit.create')}}" class="btn btn-sm btn-secondary my-2"
                               style="line-height: 1.5 !important;"><i class="fas fa-plus"></i>Add Unit</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Unit Info</h3>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered table-striped" id="department">
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th> Name</th>
                                        <th>Short Name</th>
                                        <th>Company Name</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($units))
                                        <?php $i = 1 ?>
                                        @foreach($units as $unitslist)
                                            <tr id="gid{{ $unitslist->id }}">
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $unitslist->unit_name }}</td>
                                                <td>{{ $unitslist->short_name }}</td>
                                                <td>{{ $unitslist->company_name }}</td>
                                                <td>
                                                    @csrf
                                                    <a href="{{route('unit.edit',[$unitslist->id])}}"
                                                       class="btn btn-sm btn-warning my-2" title="Edit">
                                                        <i class="fas fa-edit" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="javascript:void(0)"
                                                       onclick="deleteConfirm({{ $unitslist->id }})"
                                                       class="btn btn-sm btn-danger my-2" title="Delete">
                                                        <i class="fas fa-trash" aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td><h4>No Data Found!</h4></td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('custom_js')
    <!-- Page specific script -->
    <script>
        $(function () {
            $("#department").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#department_wrapper .col-md-6:eq(0)');
        });
        function deleteConfirm(id)
        {
            var token = $("meta[name='csrf-token']").attr("content");
            if (confirm("Are you sure to delete this record!")) {
                $.ajax({
                    url: 'unit/delete/' + id,
                    type: 'get',
                    success: function (status)
                    {
                        if(status.status==1){
                            window.location.reload();
                        }
                    }
                })
            }
        }
    </script>
@endpush

