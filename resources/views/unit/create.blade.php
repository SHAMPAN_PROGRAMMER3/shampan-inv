@extends('layouts.app')
@section('mainContent')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-7">
                        <div class="card-body flex-row justify-content-start">
                            <h3 class="text-bold text-right">Add Unit</h3>
                        </div>
                    </div>
                    <div class="col-sm-5 mt-2">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('unit.index')}}">Unit List</a></li>
                            <li class="breadcrumb-item active">Add</li>
                        </ol>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-12">
                        <div class="card card-secondary">
                            <div class="card-header">
                                <h3 class="card-title">Add Unit</h3>
                            </div>
                            <form action="{{ route('unit.store') }}" class="p-2" method="post" enctype="multipart/form-data">
                                @csrf
                                @if(!isset($company_id) && empty($company_id))
                                    <div class="form-group row">
                                        <label for="company_id" class="col-12 col-form-label">Company Name <span class="text-danger">*</span></label>
                                        <div class="col-lg-12">
                                            <select class="form-control select2" id="company_id" name="company_id" style="width: 100%;" required="required">
                                                <option value="">Select Company</option>
                                                @foreach($companies as $company)
                                                    <option value="{{ $company->id }}" {{($company->id == old('company_id'))?'selected':''}}>{{ $company->company_name }}</option>
                                                @endforeach
                                            </select>
                                            @error('company_id')
                                            <span class="text-danger ml-3 mt-1">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                @else
                                    <input type="hidden" name="company_id" value="{{$company_id}}">
                                @endif
                                <div class="form-group row">
                                    <label for="name" class="col-12 col-form-label">Unit Name <span class="text-danger">*</span></label>
                                    <div class="col-12">
                                        <input class="form-control custom-focus @error('unit_name') is-invalid @enderror" placeholder="Unit Name" id="unit_name" name="unit_name" type="text" value="{{ old('unit_name') }}" required>
                                    </div>
                                    @error('unit_name')
                                    <span class="text-danger ml-3 mt-1">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-12 col-form-label">Short Name <span class="text-danger">*</span></label>
                                    <div class="col-12">
                                        <input class="form-control custom-focus @error('short_name') is-invalid @enderror" placeholder="Short Name" id="short_name" name="short_name" type="text" value="{{ old('short_name') }}" required>
                                    </div>
                                    @error('short_name')
                                    <span class="text-danger ml-3 mt-1">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group row">
                                    <div class="col-12 mt-3 mb-2 text-right">
                                        <button type="submit" class="btn btn-primary">Save</button>
                                        <a href="{{ route('unit.index') }}" class="btn btn-secondary">Go To Back</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
