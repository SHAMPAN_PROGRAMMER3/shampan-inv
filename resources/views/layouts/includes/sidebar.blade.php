<!-- Main Sidebar Container -->
@php
    $emp = App\Models\JoinModel::findEmployee(['users.id'=>auth()->user()->id]) ;


@endphp
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('dashboard') }}" class="brand-link">
        <img src="{{ /*($emp->company_id != 0)? asset('/images/'.$emp->company_favicon) : */asset('/img/favicon.png')}}" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">{{/*($emp->company_id != 0)?$emp->company_name:*/'SHAMPAN INV'}}</span>
    </a>
<!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                @if(isset($emp->image) && !empty($emp->image))

                    <img src="{{ asset('/images/'.$emp->image)}}" class="img-circle elevation-2" alt="User Image">
                @else
                    <img src="{{ asset('/img/avatar-1577909_1280.png')}}" class="img-circle elevation-2" alt="User Image">

                @endif

            </div>
            <div class="info">
                <a class="d-block">{{ $emp->name }}</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="fa fa-users"></i>
                        <p>
                            User
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('employee.index') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>User List</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @can('products')
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="fa fa-cubes"></i>
                        <p>
                            Product Management
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('items.index') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Products</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('products.entry.index') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Product Entry</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('products.out.index') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Product Out</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('products.return.index') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Product Return List</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('products.damage.index') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Product Damage List</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan
                @can('reports')
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="fas fa-file-invoice"></i>
                        <p>
                            Report
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('entry.report.index') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Entry Report</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('out.report.index') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Out Report</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('products.entry.report.index') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Product Entry Report</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('products.out.report.index') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Product Out Report</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan
                @can('menu.admin.config')
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="fas fa-user-cog"></i>
                            <p>
                                Admin Config
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('user.list')
                                <li class="nav-item {{ \Illuminate\Support\Facades\Route::currentRouteName() == 'admin.list' ? 'active' : '' }}">
                                    <a href="{{ route('admin.list') }}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Employee List</p>
                                    </a>
                                </li>
                            @endcan
                            @can('roles')
                                <li class="nav-item {{ \Illuminate\Support\Facades\Route::currentRouteName() == 'admin.role' ? 'active' : '' }}">
                                    <a href="{{ route('admin.role') }}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Roles</p>
                                    </a>
                                </li>
                            @endcan
                            @can('permissions')

                                <li class="nav-item {{ \Illuminate\Support\Facades\Route::currentRouteName() == 'admin.permission' ? 'active' : '' }}">
                                    <a href="{{ route('admin.permission') }}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Permissions</p>
                                    </a>
                                </li>
                            @endcan
                            {{--@can('log.reports')
                                <li class="nav-item {{ \Illuminate\Support\Facades\Route::currentRouteName() == 'admin.logreport' ? 'active' : '' }}">
                                    <a href="{{ route('admin.logreport') }}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Log Report</p>
                                    </a>
                                </li>
                            @endcan--}}
                        </ul>
                    </li>
                @endcan
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
