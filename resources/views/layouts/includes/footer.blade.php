<footer class="main-footer">
    <strong>
        <div>
            Copyright
            &copy;
            <span id="copyright">
                    <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script>
                <a href="https://www.shampanit.com/" target="_blank">Shampan IT</a>.
                </span>
            All rights reserved.
        </div>
    </strong>
    <div class="float-right d-none d-sm-inline-block">
        <!--  <b>Version</b> 3.1.0 -->
    </div>
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
