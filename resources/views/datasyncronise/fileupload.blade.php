@extends('layouts.app')
@section('mainContent')

    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card-body d-flex flex-row justify-content-start">
                            <h5>Employee Info</h5>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card-body flex-row text-right">
                            <a href="{{route('data.syncronise.index')}}" class="btn btn-sm btn-secondary my-2"
                               style="line-height: 1.5 !important;"> Reset</a>

                        </div>
                    </div>
                </div>
                <!-- filter start -->
                <div class="card card-widget">
                    <form action="{{ route('file.upload.save') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="name" class="col-12 col-form-label">Select Company</label>
                                    <div class="col-12">
                                            <select class="form-control select2" id="group_id" name="group_id"
                                                    style="width: 100%;" required="required">

                                                @foreach($group as $groupdata)
                                                    <option
                                                        value="{{ $groupdata->id }}">{{ $groupdata->group_name }}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="name" class="col-12 col-form-label">Select Branch</label>
                                    <div class="col-12">
                                            <select class="form-control select2" id="branch_id" name="branch_id"
                                                    style="width: 100%;" required="required">
                                                <option value="">Select Branch</option>
                                                @foreach($branch as $bran)
                                                    <option value="{{ $bran->id }}">{{ $bran->branch_name }}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="name" class="col-12 col-form-label">Select Device</label>
                                    <div class="col-12">
                                            <select class="form-control select2" id="device_id" name="device_id"
                                                    style="width: 100%;" required="required">
                                                <option value="">Select Device</option>
                                                @foreach($device as $dev)
                                                    <option value="{{ $dev->id }}">{{ $dev->ip }}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="name" class="col-12 col-form-label">File</label>
                                    <div class="col-12">
                                            <input type="file" name="emp_att_file" class="form-control" required="required" id="emp_info">
                                    </div>
                                </div>
                                @error('emp_att_file')
                                <span class="text-success ml-3 mt-1">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="name" class="col-12 col-form-label">Save File</label>
                                    <div class="col-12">
                                            <input type="submit" name="submit" class="form-control btn-secondary" value="Save File">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
    @endsection
