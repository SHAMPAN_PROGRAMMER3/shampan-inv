<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\SuperAdminController;

use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\ItemsController;
use App\Http\Controllers\ProductEntryController;
use App\Http\Controllers\ReportsController;
use App\Http\Controllers\ProductOutController;
use App\Http\Controllers\ProductReturnController;
use App\Http\Controllers\ProductDamageController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a company which
| contains the "web" middleware company. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/monitor', [DashboardController::class, 'monitor']);
Route::get('/iframe', [DashboardController::class, 'iframe']);

/*Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');*/

Route::get('/dashboard', [DashboardController::class, 'dashboard'])->middleware(['auth'])->name('dashboard');
require __DIR__ . '/auth.php';
Route::middleware(['auth', 'verified'])->group(function () {
    //Ajax
    Route::get('getEmployeeDetails/{id}', [EmployeeController::class, 'getEmployeeDetails']);

    //Ajax
    Route::post('ajax/get-warehouse', [CompanyController::class, 'getWarehouse']);
   // Route::post('ajax/get-department', [CompanyController::class, 'getDepartment']);
    Route::post('ajax/get-designation', [DepartmentController::class, 'getDesignation']);
    Route::post('ajax/get-product', [ProductEntryController::class, 'getProduct']);
    Route::post('ajax/get-product-out', [ProductOutController::class, 'getProduct']);
   // Route::post('ajax/get-customer', [ProductEntryController::class, 'getCustomer']);

    //unit
    Route::get('unit', [UnitController::class, 'index'])->name('unit.index');
    Route::get('unit/create', [UnitController::class, 'create'])->name('unit.create');
    Route::post('unit/store', [UnitController::class, 'store'])->name('unit.store');
    Route::get('unit/edit/{id}', [UnitController::class, 'edit'])->name('unit.edit');
    Route::post('unit/update', [UnitController::class, 'update'])->name('unit.update');
    Route::get('unit/delete/{id}', [UnitController::class, 'delete'])->name('unit.delete');

    /*EMPLOYEE*/
    Route::group(['middleware' => ['can:employees']], function () {
        Route::any('employee', [EmployeeController::class, 'index'])->name('employee.index');
        Route::get('employee/create', [EmployeeController::class, 'create'])->name('employee.create');
        Route::post('employee/store', [EmployeeController::class, 'store'])->name('employee.store');
        Route::group(['middleware' => ['can:employees']], function () {
            Route::get('employee/edit/{id}', [EmployeeController::class, 'edit'])->name('employee.edit');
            Route::post('employee/update', [EmployeeController::class, 'update'])->name('employee.update');
        });
        Route::get('employee/delete/{id}', [EmployeeController::class, 'destroy'])->name('employee.delete');
        Route::post('employee/change-password', [EmployeeController::class, 'changePassword'])->name('employee.change.password');
    });

    // Items
    Route::group(['middleware' => ['can:products']], function () {
        Route::any('items', [ItemsController::class, 'index'])->name('items.index');
        Route::any('items/add', [ItemsController::class, 'create'])->name('items.create');
        Route::post('items/create', [ItemsController::class, 'store'])->name('items.store');
        Route::group(['middleware' => ['can:products.edit']], function () {
            Route::get('items/edit/{id}', [ItemsController::class, 'edit'])->name('items.edit');
            Route::post('items/update', [ItemsController::class, 'update'])->name('items.update');
        });
        Route::group(['middleware' => ['can:products.delete']], function () {
            Route::get('items/delete/{id}', [ItemsController::class, 'delete'])->name('items.delete');
        });
    });

    //product entry
    Route::group(['middleware' => ['can:product.entry']], function () {
        Route::get('products-entry', [ProductEntryController::class, 'index'])->name('products.entry.index');
        Route::get('products-entry/view/{id}', [ProductEntryController::class, 'show'])->name('products.entry.show');
        Route::get('products-entry/create', [ProductEntryController::class, 'create'])->name('products.entry.create');
        Route::post('products-entry/store', [ProductEntryController::class, 'store'])->name('products.entry.store');

        Route::group(['middleware' => ['can:product.entry.edit']], function () {
            Route::get('products-entry/edit/{id}', [ProductEntryController::class, 'edit'])->name('products.entry.edit');
            Route::post('products-entry/update', [ProductEntryController::class, 'update'])->name('products.entry.update');
        });

    });

    //product out
    Route::group(['middleware' => ['can:product.out']], function () {
        Route::get('products-out', [ProductOutController::class, 'index'])->name('products.out.index');
        Route::get('products-out/view/{id}', [ProductOutController::class, 'show'])->name('products.out.show');
        Route::get('products-out/create', [ProductOutController::class, 'create'])->name('products.out.create');
        Route::post('products-out/store', [ProductOutController::class, 'store'])->name('products.out.store');
    });

    //product return
    Route::group(['middleware' => ['can:product.return']], function () {
        Route::get('products-return', [ProductReturnController::class, 'index'])->name('products.return.index');
        Route::get('products-return/view/{id}', [ProductReturnController::class, 'show'])->name('products.return.show');
        Route::get('products-return/create/{id}', [ProductReturnController::class, 'create'])->name('products.return.create');
        Route::post('products-return/store', [ProductReturnController::class, 'store'])->name('products.return.store');
    });

    //product damage
    Route::group(['middleware' => ['can:product.damage']], function () {
        Route::get('products-damage', [ProductDamageController::class, 'index'])->name('products.damage.index');
        Route::get('products-damage/create/{id}', [ProductDamageController::class, 'create'])->name('products.damage.create');
        Route::post('products-damage/store', [ProductDamageController::class, 'store'])->name('products.damage.store');
        Route::get('products-damage/view/{id}', [ProductDamageController::class, 'view'])->name('products.damage.show');
    });

    //reports
    Route::group(['middleware' => ['can:reports']], function () {
        Route::any('entry-report', [ReportsController::class, 'index'])->name('entry.report.index');
        Route::any('out-report', [ReportsController::class, 'proOut'])->name('out.report.index');
        Route::any('products-entry-report', [ReportsController::class, 'itemEntry'])->name('products.entry.report.index');
        Route::any('products-out-report', [ReportsController::class, 'itemOut'])->name('products.out.report.index');
    });

    Route::post('policestation/filter', [PolicestationController::class, 'policestationFilter'])->name('policestation.filter');
    Route::post('district/filter', [DistrictController::class, 'districtFilter'])->name('district.filter');

    /*ADMIN CONFIG*/
    Route::group(['middleware' => ['can:menu.admin.config']], function () {
        Route::group(['middleware' => ['can:roles']], function () {
            Route::get('role', [SuperAdminController::class, 'adminRole'])->name('admin.role');
            Route::get('role/create', [SuperAdminController::class, 'roleCreate'])->name('role.create');
            Route::post('save/role', [SuperAdminController::class, 'saveRole'])->name('role.save');
            Route::get('role/edit/{id}', [SuperAdminController::class, 'roleEdit']);
           // Route::get('roleAssign/{id}', [SuperAdminController::class, 'roleAssign']);
            Route::post('assign/role', [SuperAdminController::class, 'assignRole']);
        });
        Route::group(['middleware' => ['can:permissions']], function () {
            Route::get('permission', [SuperAdminController::class, 'adminPermission'])->name('admin.permission');
            Route::get('permission/create', [SuperAdminController::class, 'permissionCreate'])->name('permission.create');
            Route::post('save/permission', [SuperAdminController::class, 'savePermission'])->name('permission.save');
            Route::post('save/childpermission', [SuperAdminController::class, 'saveChildPerm'])->name('childpermission.save');
            Route::get('permission/edit/{id}', [SuperAdminController::class, 'permEdit']);
            Route::get('childperm/edit/{parent_id}/{id}', [SuperAdminController::class, 'childpermEdit']);
            //Route::get('permission/edit/{id}',[SuperAdminController::class,'permissionEdit']);
            Route::get('permission/assign/{id}', [SuperAdminController::class, 'permissionAssign']);
            Route::post('assign/permission', [SuperAdminController::class, 'assignPermission'])->name('assign.permission');
        });
        Route::group(['middleware' => ['can:user.list']], function () {
            Route::get('admin/list', [SuperAdminController::class, 'adminList'])->name('admin.list');
            Route::post('admin/list/filter', [SuperAdminController::class, 'adminListFilter'])->name('admin.list.filter');
            Route::get('admin/create', [SuperAdminController::class, 'adminCreate'])->name('admin.create');
            Route::post('save/admin', [SuperAdminController::class, 'saveAdmin'])->name('admin.save');
            Route::get('admin/edit/{id}', [SuperAdminController::class, 'adminEdit']);
            Route::get('roleAssign/{id}', [SuperAdminController::class, 'roleAssign']);
        });
        Route::group(['middleware' => ['can:log.reports']], function () {
            Route::any('logreport', [SuperAdminController::class, 'adminlogreport'])->name('admin.logreport');
        });
    });
    Route::post('ajax/company', [EmployeeController::class, 'ajaxSearchGroup']);
    Route::post('ajax/department', [EmployeeController::class, 'ajaxSearchDepartment']);
    Route::post('ajax/designation', [EmployeeController::class, 'ajaxSearchEmpDesignation']);
    Route::post('ajax/leave_enable_check', [EmployeeLeaveController::class, 'ajaxLeaveEnableCheck']);
    Route::post('ajax/leave_application_check', [EmployeeLeaveController::class, 'ajaxLeaveAppliCheck']);
    Route::post('ajax/leave_remaining_check', [EmployeeLeaveController::class, 'ajaxLeaveRemainingCheck']);
    Route::post('ajax/deventload', [EventController::class, 'ajaxEventLoadData']);

});
