<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AttendanceMachineController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a company which
| is assigned the "api" middleware company. Enjoy building your API!
|
*/
//Route::get('/machine/data', [AttendanceMachineController::class, 'machineData']);

/*Route::company(['middleware' => 'auth:api'], function() {

   Route::get('/machine/data', [AttendanceMachineController::class, 'machineData']);
});*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


