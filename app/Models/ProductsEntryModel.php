<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductsEntryModel extends Model
{
    use HasFactory;
    /**
     * @var string
     */
    public $table = 'stock_entry';

    /**
     * @var bool
     */
    public $timestamps = false;
}
