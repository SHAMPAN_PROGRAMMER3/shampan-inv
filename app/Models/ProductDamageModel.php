<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductDamageModel extends Model
{
    use HasFactory;
    /**
     * @var string
     */
    public $table = 'product_damage';

    /**
     * @var bool
     */
    public $timestamps = false;
}
