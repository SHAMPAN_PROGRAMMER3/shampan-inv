<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductReturnDetailsModel extends Model
{
    use HasFactory;
    /**
     * @var string
     */
    public $table = 'product_return_details';

    /**
     * @var bool
     */
    public $timestamps = false;
}
