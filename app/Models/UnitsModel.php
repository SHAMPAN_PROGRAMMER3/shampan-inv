<?php
/**
 * Shampan IT Solutions
 * Copyright (C) 2021 ShampanIT <info@shampanit.com>
 *
 * @category ShampanIT
 * @package Shampan_Store
 * @copyright Copyright (c) 2021 ShampanIT (http://www.shampanit.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author ShampanIT <info@shampanit.com>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UnitsModel extends Model
{
    use HasFactory;
    /**
     * @var string
     */
    public $table = 'unit';

    /**
     * @var bool
     */
    public $timestamps = false;
}
