<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductsEntryDetailsModel extends Model
{
    use HasFactory;
    /**
     * @var string
     */
    public $table = 'stock_entry_details';

    /**
     * @var bool
     */
    public $timestamps = false;
}
