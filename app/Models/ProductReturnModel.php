<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductReturnModel extends Model
{
    use HasFactory;
    /**
     * @var string
     */
    public $table = 'product_return';

    /**
     * @var bool
     */
    public $timestamps = false;
}
