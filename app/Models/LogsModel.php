<?php
/**
 * Shampan IT Solutions
 * Copyright (C) 2021 ShampanIT <info@shampanit.com>
 *
 * @category ShampanIT
 * @package Shampan_Store
 * @copyright Copyright (c) 2021 ShampanIT (http://www.shampanit.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author ShampanIT <info@shampanit.com>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class LogsModel extends Model
{
    use HasFactory;
    /**
     * @var string
     */
    public $table = 'user_log';

    /**
     * @var bool
     */
    public $timestamps = false;

    public static function addLog($data,$table,$event)
    {
        $logData['created_at'] = date( "Y-m-d h:i:s");;
        $logData['user_id'] = auth()->user()->id;
        $logData['table_name'] = $table;
        $logData['url'] = URL::current();
        $logData['event'] = $event;
        $logData['data'] = json_encode($data);
        $logData['details'] = $event.' by '.auth()->user()->name;

        $saveDate = LogsModel::insert($logData);
        return $saveDate;
    }

}
