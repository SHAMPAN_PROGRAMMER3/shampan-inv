<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductDamageDetailsModel extends Model
{
    use HasFactory;
    /**
     * @var string
     */
    public $table = 'product_damage_details';

    /**
     * @var bool
     */
    public $timestamps = false;
}
