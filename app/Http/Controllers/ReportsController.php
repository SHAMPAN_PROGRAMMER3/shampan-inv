<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
use Illuminate\Support\Carbon;
use RealRashid\SweetAlert\Facades\Alert;

class ReportsController extends Controller
{
    public function index(Request $request)
    {
        $data=[];
        $arr=[];
        if($request->get('entry_start_date') != null || $request->get('entry_end_date') != null) {
            if ($request->get('entry_start_date')) {
                $arr[] = ['stock_entry.entry_date','>=',$request->get('entry_start_date')];
            }
            if ($request->get('entry_end_date')) {
                $arr[] = ['stock_entry.entry_date','<=',$request->get('entry_end_date')];
            }
            $data['requests'] = $request->all();

            $arr[]= ['stock_entry.status','=',1];
            $data['productEntryList'] = DB::table('stock_entry')
                ->leftjoin('users','stock_entry.entry_by','=','users.id')
                ->select('stock_entry.*','users.name as emp_name')
                ->where($arr)
                ->orderBy('stock_entry.created_at','desc')
                ->get();
            if($request->get('submit') == 'pdf')
            {
                $pdfFileName = 'product-entry-report-'.Carbon::now()->format('YmdHis').'.pdf';
                $pdf = PDF::loadView('reports.entry-pdf',$data)->setPaper('a4', 'portrait')->setOptions(['defaultFont' => 'sans-serif']);
                return $pdf->stream($pdfFileName);
            }
            else{
                return view('reports.entry',$data);
            }
        }
        else{
            if($request->all())
            {
                Alert::toast('Please select Date.','error')->width('570px');
            }
            return view('reports.entry');
        }
    }
    public function proOut(Request $request)
    {
        $data=[];
        $arr=[];
        if($request->get('entry_start_date') != null || $request->get('entry_end_date') != null) {
            if ($request->get('entry_start_date')) {
                $arr[] = ['stock_out_entry.out_date','>=',$request->get('entry_start_date')];
            }
            if ($request->get('entry_end_date')) {
                $arr[] = ['stock_out_entry.out_date','<=',$request->get('entry_end_date')];
            }
            $data['requests'] = $request->all();

            $arr[]= ['stock_out_entry.status','=',1];
            $data['productOutList'] = DB::table('stock_out_entry')
                ->leftjoin('users','stock_out_entry.out_by','=','users.id')
                ->select('stock_out_entry.*','users.name as emp_name')
                ->where($arr)
                ->orderBy('stock_out_entry.created_at','desc')
                ->get();
            if($request->get('submit') == 'pdf')
            {
                $pdfFileName = 'product-out-report-'.Carbon::now()->format('YmdHis').'.pdf';
                $pdf = PDF::loadView('reports.out-pdf',$data)->setPaper('a4', 'portrait')->setOptions(['defaultFont' => 'sans-serif']);
                return $pdf->stream($pdfFileName);
            }
            else{
                return view('reports.out',$data);
            }
        }
        else{
            if($request->all())
            {
                Alert::toast('Please select Date.','error')->width('570px');
            }
            return view('reports.out');
        }
    }
    public function itemEntry(Request $request)
    {
        $data['allProducts'] = DB::table('items')
            ->select('items.*')
            ->where('items.status','=',1)
            ->get();

        $arrDate=[];
        if($request->get('entry_start_date') != null || $request->get('entry_end_date') != null || $request->get('item_id') != null) {
            if ($request->get('entry_start_date')) {
                $arrDate[] = ['stock_entry_details.created_at','>=',$request->get('entry_start_date')];
            }
            if ($request->get('entry_end_date')) {
                $arrDate[] = ['stock_entry_details.created_at','<=',$request->get('entry_end_date')];
            }
            foreach ($request->get('item_id') as $itemId)
            {
                $arr=[];
                if ($request->get('item_id')) {
                    $arr[] = ['stock_entry_details.product_id','=',$itemId];
                }
                $data['requests'] = $request->all();
                $arr[]= ['stock_entry_details.status','=',1];
                $dataStockEntry = DB::table('stock_entry_details')
                    ->select('stock_entry_details.*')
                    ->where($arrDate)
                    ->where($arr)
                    ->get();
                if(!empty($dataStockEntry[0]))
                {
                    $data['productEntryList'][$itemId] = $dataStockEntry;
                }
            }
            if($request->get('submit') == 'pdf')
            {
                $pdfFileName = 'product-entry-report-'.Carbon::now()->format('YmdHis').'.pdf';
                $pdf = PDF::loadView('reports.product-entry-pdf',$data)->setPaper('a4', 'portrait')->setOptions(['defaultFont' => 'sans-serif']);
                return $pdf->stream($pdfFileName);
            }
        else{
                return view('reports.product-entry',$data);
            }
        }
    else{
            if($request->all())
            {
                Alert::toast('Please select Date.','error')->width('570px');
            }
            return view('reports.product-entry',$data);
        }
    }
    public function itemOut(Request $request)
    {
        $data['allProducts'] = DB::table('items')
            ->select('items.*')
            ->where('items.status','=',1)
            ->get();

        $arrDate=[];
        if($request->get('entry_start_date') != null || $request->get('entry_end_date') != null || $request->get('item_id') != null) {
            if ($request->get('entry_start_date')) {
                $arrDate[] = ['stock_out_details.created_at','>=',$request->get('entry_start_date')];
            }
            if ($request->get('entry_end_date')) {
                $arrDate[] = ['stock_out_details.created_at','<=',$request->get('entry_end_date')];
            }
            foreach ($request->get('item_id') as $itemId)
            {
                $arr=[];
                if ($request->get('item_id')) {
                    $arr[] = ['stock_out_details.product_id','=',$itemId];
                }
                $data['requests'] = $request->all();
                $arr[]= ['stock_out_details.status','=',1];
                $dataStockEntry = DB::table('stock_out_details')
                    ->select('stock_out_details.*')
                    ->where($arrDate)
                    ->where($arr)
                    ->get();
                if(!empty($dataStockEntry[0]))
                {
                    $data['productEntryList'][$itemId] = $dataStockEntry;
                }
            }
            if($request->get('submit') == 'pdf')
            {
                $pdfFileName = 'product-out-report-'.Carbon::now()->format('YmdHis').'.pdf';
                $pdf = PDF::loadView('reports.product-out-pdf',$data)->setPaper('a4', 'portrait')->setOptions(['defaultFont' => 'sans-serif']);
                return $pdf->stream($pdfFileName);
            }
            else{
                return view('reports.product-out',$data);
            }
        }
        else{
            if($request->all())
            {
                Alert::toast('Please select Date.','error')->width('570px');
            }
            return view('reports.product-out',$data);
        }
    }
}
