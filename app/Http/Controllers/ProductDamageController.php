<?php

namespace App\Http\Controllers;

use App\Models\ProductDamageModel;
use App\Models\ProductDamageDetailsModel;
use App\Models\ProductOutDetailsModel;
use App\Models\ProductOutHistoryModel;
use App\Models\ProductOutModel;
use App\Models\ProductReturnModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use DB;

class ProductDamageController extends Controller
{
    //
    public function index(){
        $data['productDamageList'] = DB::table('product_damage')
            ->leftjoin('users','product_damage.damage_by','=','users.id')
            ->where('product_damage.status','=',1)
            ->select('product_damage.*','users.name as emp_name')
            ->orderBy('product_damage.created_at','desc')
            ->get();
        return view('products-damage.index', $data);
    }

    public function create($id)
    {
        $where['stock_out_entry.id'] = $id ;
        $data['productOutDetails'] = ProductOutModel::where($where)
            ->leftjoin('stock_out_details','stock_out_entry.id','=','stock_out_details.stock_out_id')
            ->leftjoin('items','stock_out_details.product_id','=','items.id')
            ->select('stock_out_details.*', 'items.item_name', 'items.item_code', 'stock_out_entry.total_price as grand_total','stock_out_entry.id as out_id')
            ->get();
        $history = ProductOutDetailsModel::where([['stock_out_details.stock_out_id','=',$id],['product_out_history.returnable_qty','>',0]])
            ->leftjoin('product_out_history','product_out_history.out_details_id','=','stock_out_details.id')
            ->select('product_out_history.*')
            ->get();
        $arr=[];
        foreach($history as $key=>$value)
        {
            $arr[$value->out_details_id][$key]['history_id'] = $value->id;
            $arr[$value->out_details_id][$key]['entry_details_id'] = $value->entry_details_id;
            $arr[$value->out_details_id][$key]['unit_price'] = $value->unit_price;
            $arr[$value->out_details_id][$key]['qty'] = $value->returnable_qty;
        }
        $data['history']=$arr;
        /*Previous data collection for damage and return*/
        $previousReturnData = ProductReturnModel::where('product_return.entry_out_id',$id)
            ->leftjoin('product_return_details','product_return.id','=','product_return_details.product_return_id')
            ->select('product_return_details.*')
            ->get();
        $arrReturn=[];
        foreach($previousReturnData as $returnData)
        {
            if(isset($arrReturn[$returnData->product_id]))
            {
                $arrReturn[$returnData->product_id] = $arrReturn[$returnData->product_id] + $returnData->product_qty;
            }
            else{
                $arrReturn[$returnData->product_id] = $returnData->product_qty;
            }
        }
        $data['return']=$arrReturn;

        $previousDamageData = ProductDamageModel::where([['product_damage.entry_in_out_id','=',$id],['product_damage.status','=',1]])
            ->leftjoin('product_damage_details','product_damage.id','=','product_damage_details.product_damage_id')
            ->select('product_damage_details.*')
            ->get();

        $arrDamage=[];
        foreach($previousDamageData as $damageData)
        {
            if(isset($arrDamage[$damageData->product_id]))
            {
                $arrDamage[$damageData->product_id] = $arrDamage[$damageData->product_id] + $damageData->product_qty;
            }
            else{
                $arrDamage[$damageData->product_id] = $damageData->product_qty;
            }
        }
        $data['damage']=$arrDamage;

        if(empty($arr))
        {
            Alert::toast('There are no products to damage.','error')->width('570px');
            return redirect()->route('products.out.index');
        }
        return view('products-damage.create', $data);
    }
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            //validation
            $arrProduct = [];
            foreach ($request->get('products') as $key=>$value)
            {
                if($value['damage_qty'] > 0 && $value['damage_qty']!= null && ($value['damage_qty']<=($value['total_qty']- $value['previous_return']-$value['previous_damage'])) )
                {
                    $arrProduct[$key] = $value;
                }
            }
            //data save to damage table
            if(!empty($arrProduct)) {
                $productDamageModel = new ProductDamageModel();
                $productDamageModel->damage_by = auth()->user()->id;
                $productDamageModel->product_type_qty = count($arrProduct);
                $productDamageModel->serial_no = strtotime(date('Y-m-d H:i:s'));
                $productDamageModel->damage_date = date('Y-m-d');
                $productDamageModel->entry_in_out_id = $value['out_id'];
                $productDamageModel->slug = 'out';
                $productDamageModel->total_price = 0;
                $productDamageModel->status = 1;
                $productDamageModel->created_at = Carbon::now();
                $productDamageModel->save();
                $productDamageId = $productDamageModel->id;

                $arrDamageProductDetails = [];
                $totalPrice = 0;
                /*data save in damage_details table*/
                foreach ($arrProduct as $key => $product) {
                    $history = [];
                    $arrDamageProductDetails[$key]['product_damage_id'] = $productDamageId;
                    $arrDamageProductDetails[$key]['product_id'] = $product['item_id'];
                    $arrDamageProductDetails[$key]['created_at'] = Carbon::now();
                    $arrDamageProductDetails[$key]['product_qty'] = $product['damage_qty'];
                    $arrDamageProductDetails[$key]['item_name'] = $product['item_name'];
                    $arrDamageProductDetails[$key]['item_code'] = $product['item_code'];
                    $arrDamageProductDetails[$key]['status'] = 1;

                    $historyOut = json_decode($product['qty_price_history']);
                    $damageQty = $product['damage_qty'];
                    $productTotalPrice = 0;

                    foreach ($historyOut as $value) {
                    if($damageQty <= $value->qty && $damageQty > 0){
                        $productTotalPrice = $productTotalPrice + ($damageQty * $value->unit_price);

                        $history[] = [
                            'id'=>$value->entry_details_id,
                            'qty'=>$damageQty,
                            'unit_price'=> $value->unit_price
                        ];
                        ProductOutHistoryModel::where('id',$value->history_id)->update(['returnable_qty'=> ($value->qty - $damageQty)]);
                        break;
                    }

                    elseif($damageQty > $value->qty && $damageQty > 0) {
                          $damageQty = $damageQty - $value->qty;
                        $productTotalPrice = $productTotalPrice + ($value->qty * $value->unit_price);
                        $history[] = [
                            'id' => $value->entry_details_id,
                            'qty' => $value->qty,
                            'unit_price' => $value->unit_price
                        ];
                        ProductOutHistoryModel::where('id',$value->history_id)->update(['returnable_qty'=> 0]);
                    }
                }

                $totalPrice = $totalPrice + $productTotalPrice;
                    $arrDamageProductDetails[$key]['total_price'] = $productTotalPrice;
                    $arrDamageProductDetails[$key]['qty_price_history'] = json_encode($history);

            }
                if (count($arrDamageProductDetails) > 0) {
                    ProductDamageDetailsModel::insert($arrDamageProductDetails);
                }
                ProductDamageModel::where('id', $productDamageId)->update(['total_price' => $totalPrice]);
            DB::commit();
            Alert::toast('Data successfully Inserted', 'success')->width('375px');
            }else{
                Alert::toast('Please give valid quantity of products','error')->width('570px');
            }
        }catch (\Exception $e) {
            DB::rollback();
            Alert::toast('Data did not Insert successfully','error')->width('570px');
        }
        return redirect()->route('products.damage.index');
    }
    public function view($id)
    {
        $where['product_damage.id'] = $id ;
        $data['productDamageDetails'] = ProductDamageModel::where($where)
            ->leftjoin('product_damage_details','product_damage.id','=','product_damage_details.product_damage_id')
            ->select('product_damage_details.*','product_damage.total_price as grand_total')
            ->get();
        return view('products-damage.view', $data);

    }
}
