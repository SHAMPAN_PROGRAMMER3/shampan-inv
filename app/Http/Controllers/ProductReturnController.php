<?php

namespace App\Http\Controllers;

use App\Models\ItemsModel;
use App\Models\ProductDamageModel;
use App\Models\ProductOutDetailsModel;
use App\Models\ProductOutHistoryModel;
use App\Models\ProductOutModel;
use App\Models\ProductReturnDetailsModel;
use App\Models\ProductReturnModel;
use App\Models\ProductsEntryDetailsModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class ProductReturnController extends Controller
{
    //
    public function index(){

        $data['productReturnList'] = DB::table('product_return')
            ->leftjoin('users','product_return.return_by','=','users.id')
            ->where('product_return.status','=',1)
            ->select('product_return.*','users.name as emp_name')
            ->orderBy('product_return.created_at','desc')
            ->get();
        return view('products-return.index', $data);
    }
    public function create($id)
    {
        $where['stock_out_entry.id'] = $id ;
        $data['productOutDetails'] = ProductOutModel::where($where)
            ->leftjoin('stock_out_details','stock_out_entry.id','=','stock_out_details.stock_out_id')
            ->leftjoin('items','stock_out_details.product_id','=','items.id')
            ->select('stock_out_details.*', 'items.item_name', 'items.item_code', 'stock_out_entry.total_price as grand_total')
            ->get();
        $history = ProductOutDetailsModel::where([['stock_out_details.stock_out_id','=',$id],['product_out_history.returnable_qty','>',0]])
            ->leftjoin('product_out_history','product_out_history.out_details_id','=','stock_out_details.id')
            ->select('product_out_history.*')
            ->get();
        $arr=[];
        foreach($history as $key=>$value)
        {
            $arr[$value->out_details_id][$key]['history_id'] = $value->id;
            $arr[$value->out_details_id][$key]['entry_details_id'] = $value->entry_details_id;
            $arr[$value->out_details_id][$key]['unit_price'] = $value->unit_price;
            $arr[$value->out_details_id][$key]['qty'] = $value->returnable_qty;
        }
        $data['history']=$arr;

        /*Previous data collection for damage and return*/
        $previousReturnData = ProductReturnModel::where('product_return.entry_out_id',$id)
        ->leftjoin('product_return_details','product_return.id','=','product_return_details.product_return_id')
        ->select('product_return_details.*')
        ->get();
        $arrReturn=[];
        foreach($previousReturnData as $returnData)
        {
            if(isset($arrReturn[$returnData->product_id]))
            {
                $arrReturn[$returnData->product_id] = $arrReturn[$returnData->product_id] + $returnData->product_qty;
            }
            else{
                $arrReturn[$returnData->product_id] = $returnData->product_qty;
            }
        }
        $data['return']=$arrReturn;

        $previousDamageData = ProductDamageModel::where([['product_damage.entry_in_out_id','=',$id],['product_damage.status','=',1]])
        ->leftjoin('product_damage_details','product_damage.id','=','product_damage_details.product_damage_id')
        ->select('product_damage_details.*')
        ->get();

        $arrDamage=[];
        foreach($previousDamageData as $damageData)
        {
            if(isset($arrDamage[$damageData->product_id]))
            {
                $arrDamage[$damageData->product_id] = $arrDamage[$damageData->product_id] + $damageData->product_qty;
            }
            else{
                $arrDamage[$damageData->product_id] = $damageData->product_qty;
            }
        }
        $data['damage']=$arrDamage;

        if(empty($arr))
        {
            Alert::toast('There are no products to return.','error')->width('570px');
            return redirect()->route('products.out.index');
        }
        return view('products-return.create', $data);
    }
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            /*product data validation*/
            $arrProduct=[];
            foreach($request->get('products') as $value)
            {
                if($value['qty'] > 0 && $value['qty']!= null && ($value['qty']<=($value['total_qty']- $value['previous_return']-$value['previous_damage'])) ){
                    $arrProduct[]=$value;
                }
            }
            //dd($arrProduct);
            if(!empty($arrProduct)) {
                /*Data save in Return table*/
                $productReturnModel = new ProductReturnModel();
                $productReturnModel->return_by = auth()->user()->id;
                $productReturnModel->product_type_qty = count($arrProduct);
                $productReturnModel->entry_out_id = $request->get('out_id');
                $productReturnModel->serial_no = strtotime(date('Y-m-d H:i:s'));
                $productReturnModel->return_date = date('Y-m-d');
                $productReturnModel->status = 1;
                $productReturnModel->created_at = Carbon::now();
                $productReturnModel->save();
                $productReturnId = $productReturnModel->id;

                /*set return table details data*/
                $arrReturnProductDetails = [];
                $totalPrice=0;
                foreach ($arrProduct as $key => $product)
                {
                    $history=[];
                    $arrReturnProductDetails[$key]['product_return_id'] = $productReturnId;
                    $arrReturnProductDetails[$key]['product_id'] = $product['product_id'];
                    $arrReturnProductDetails[$key]['product_qty'] = $product['qty'];
                    $arrReturnProductDetails[$key]['item_name'] = $product['item_name'];
                    $arrReturnProductDetails[$key]['item_code'] = $product['item_code'];
                    $arrReturnProductDetails[$key]['created_at'] = Carbon::now();
                    $arrReturnProductDetails[$key]['status'] = 1;

                    // price calculation and qty,total_price update in entry table
                    $dataHistory = json_decode($product['qty_price_history']);
                    $productQty = $product['qty'];
                    $totalReturnProductPrice=0;
                    foreach($dataHistory as $key2 => $value)
                    {
                        if($productQty <= $value->qty && $productQty>0)
                        {
                            $totalReturnProductPrice = $totalReturnProductPrice + ($productQty * $value->unit_price);
                            $history[] = [
                                'id'=>$value->entry_details_id,
                                'qty'=>$productQty,
                                'unit_price'=> $value->unit_price
                            ];

                            /*update entry details*/
                            $productEntryData=ProductsEntryDetailsModel::find($value->entry_details_id);
                            ProductsEntryDetailsModel::where('id',$value->entry_details_id)->update(['sellable_qty'=>($productEntryData->sellable_qty + $productQty)]);
                            /*update history data*/
                            ProductOutHistoryModel::where('id',$value->history_id)->update(['returnable_qty'=> ($value->qty - $productQty)]);
                            break;
                        }
                        elseif($productQty > $value->qty && $productQty>0){
                            $productQty = $productQty - $value->qty;
                            $totalReturnProductPrice = $totalReturnProductPrice + ($value->qty * $value->unit_price);

                            $history[] = [
                                'id'=>$value->entry_details_id,
                                'qty'=>$value->qty,
                                'unit_price'=> $value->unit_price
                            ];

                            /*update entry details*/
                            $productEntryData=ProductsEntryDetailsModel::find($value->entry_details_id);
                            ProductsEntryDetailsModel::where('id',$value->entry_details_id)->update(['sellable_qty'=>($productEntryData->sellable_qty + $value->qty)]);

                            /*update history data*/
                            ProductOutHistoryModel::where('id',$value->history_id)->update(['returnable_qty'=> 0]);
                        }
                    }

                    $totalPrice = $totalPrice + $totalReturnProductPrice;
                    $arrReturnProductDetails[$key]['total_price'] = $totalReturnProductPrice;
                    $arrReturnProductDetails[$key]['qty_price_history'] = json_encode($history);

                    //Product total stock update
                    $item = ItemsModel::find($product['product_id']);
                    ItemsModel::where('id', $product['product_id'])->update(['total_qty' => ($item->total_qty + $product['qty']), 'total_price' => ($item->total_price + $totalReturnProductPrice)]);
                }
                if (count($arrReturnProductDetails) > 0) {
                    ProductReturnDetailsModel::insert($arrReturnProductDetails);
                }
                //update stock total price data;
                ProductReturnModel::where('id',$productReturnId)->update(['total_price' => $totalPrice]);
                DB::commit();
                Alert::toast('Data successfully Inserted', 'success')->width('375px');
            }else{
                Alert::toast('Please give valid quantity of products','error')->width('570px');
            }
        }catch (\Exception $e) {
            DB::rollback();
            Alert::toast('Data did not Insert successfully','error')->width('570px');
        }
        return redirect()->route('products.return.index');
    }

    public function show($id)
    {
        $where['product_return.id'] = $id ;
        $data['productReturnDetails'] = ProductReturnModel::where($where)
            ->leftjoin('product_return_details','product_return.id','=','product_return_details.product_return_id')
            ->select('product_return_details.*','product_return.total_price as grand_total')
            ->get();
        return view('products-return.view', $data);

    }
}
