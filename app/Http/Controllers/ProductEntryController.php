<?php

namespace App\Http\Controllers;

use App\Models\ProductsEntryDetailsModel;
use App\Models\ProductsEntryModel;
use App\Models\ItemsModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class ProductEntryController extends Controller
{
	public function index()
	{
        $data['productEntryList'] = DB::table('stock_entry')
            ->leftjoin('users','stock_entry.entry_by','=','users.id')
            ->where('stock_entry.status','=',1)
            ->select('stock_entry.*','users.name as emp_name')
            ->orderBy('created_at','desc')
            ->get();
		return view('products-entry.index', $data);
	}
	public function create()
	{
		return view('products-entry.create');
	}
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            //validation
            $arrProduct = [];
            foreach ($request->get('products') as $key => $value) {
                if ($value['qty'] != Null && $value['item_price'] != Null && $value['qty'] > 0 && $value['item_price'] > 0) {
                    $arrProduct[$key] = $value;
                }
            }
            if(!empty($arrProduct)) {
            //sales order save
            $salesOrderModel = new ProductsEntryModel();
            $salesOrderModel->entry_by = auth()->user()->id;
            $salesOrderModel->total_price = 0;
            $salesOrderModel->product_type_qty = count($arrProduct);
            $salesOrderModel->serial_no = strtotime(date('Y-m-d H:i:s'));
            $salesOrderModel->entry_date = date('Y-m-d');
            $salesOrderModel->status = 1;
            $salesOrderModel->created_at = Carbon::now();
            $salesOrderModel->save();

            $salesOrderId = $salesOrderModel->id;
            //sales order details save
            $arr = [];
            $itemIdArr = [];
            $grandTotal=0;
            foreach ($arrProduct as $key => $product) {
                $arr[$key]['stock_entry_id'] = $salesOrderId;
                $arr[$key]['product_id'] = $product['item_id'];
                $arr[$key]['created_at'] = Carbon::now();
                $arr[$key]['sellable_qty'] = $product['qty'];
                $arr[$key]['item_name'] = $product['item_name'];
                $arr[$key]['item_code'] = $product['item_code'];
                $arr[$key]['total_qty'] = $product['qty'];
                $arr[$key]['unit_price'] = $product['item_price'];
                $arr[$key]['total_price'] = $product['item_price'] * $product['qty'];

                $itemIdArr[$product['item_id']]['qty'] = $product['qty'];
                $itemIdArr[$product['item_id']]['total_price'] = $product['item_price'] * $product['qty'];
                $grandTotal = $grandTotal + $arr[$key]['total_price'];
            }
            //dd($arr);
            if (count($arr) > 0) {

                ProductsEntryDetailsModel::insert($arr);
            }
                ProductsEntryModel::where('id', $salesOrderId)->update([
                    'total_price' => $grandTotal,
                ]);

            if (count($itemIdArr) > 0) {
                foreach ($itemIdArr as $id => $value) {
                    $itemData = ItemsModel::where('id', $id)->first();
                    $totalQty = $itemData->total_qty + $value['qty'];
                    $totalPrice = $itemData->total_price + $value['total_price'];
                    ItemsModel::where('id', $id)->update([
                        'total_qty' => $totalQty,
                        'total_price' => $totalPrice,
                    ]);
                }
            }
            DB::commit();
            Alert::toast('Data successfully Inserted', 'success')->width('375px');
        }
            else{
                Alert::toast('Data did not Insert successfully','error')->width('570px');
            }
        }catch (\Exception $e) {
            DB::rollback();
            Alert::toast('Data did not Insert successfully','error')->width('570px');
        }
        return redirect()->route('products.entry.index');

    }
    public function show($id)
    {
        $where['stock_entry.id'] = $id ;
        $data['productEntryDetails'] = ProductsEntryModel::where($where)
            ->leftjoin('stock_entry_details','stock_entry.id','=','stock_entry_details.stock_entry_id')
            ->leftjoin('items','stock_entry_details.product_id','=','items.id')
            ->select('stock_entry_details.*', 'items.item_name', 'items.item_code', 'stock_entry.total_price as grand_total')
            ->get();
        return view('products-entry.view', $data);
    }
	public function getProduct(Request $request)
	{
        $where['items.item_code'] = $request->get('item_code');
        $where['items.status'] = 1;
        $data = DB::table('items')
            ->where($where)
            ->select('items.*')
            ->get();
        return $data;
	}
    public function edit($id)
    {
        $where['stock_entry.id'] = $id ;
        $data['productEntryDetails'] = ProductsEntryModel::where($where)
            ->leftjoin('stock_entry_details','stock_entry.id','=','stock_entry_details.stock_entry_id')
            ->leftjoin('items','stock_entry_details.product_id','=','items.id')
            ->select('stock_entry_details.*', 'items.item_name', 'items.item_code', 'stock_entry.total_price as grand_total', 'stock_entry.id as entry_id')
            ->get();
        //dd($data);
        return view('products-entry.edit', $data);
    }
    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->get('products') as $key => $value) {
                if ($value['item_qty'] == Null || $value['item_price'] == Null || $value['item_qty'] < 0 || $value['item_price'] < 0) {
                    Alert::toast('please give valid data', 'error')->width('570px');
                    return redirect()->route('products.entry.index');
                }
            }
            $salesOrderId = $request->get('entry_id');
            $entryTotalPrice = 0;
            if ($salesOrderId) {
                foreach ($request->get('products')  as $key => $product) {
                    $data = ProductsEntryDetailsModel::find($product['id']);
                    if ($product['item_qty'] < $data->total_qty) {
                        $deffer = $data->total_qty - $product['item_qty'];
                        if ($deffer > $data->sellable_qty) {
                            Alert::toast('product already out. can not update', 'error')->width('570px');
                            return redirect()->route('products.entry.index');
                        } else {
                            $sellable = $data->sellable_qty - ($data->total_qty - $product['item_qty']);
                            //product qty and price update for decrease qty or price
                            $productTotalPrice = $product['item_price'] * $product['item_qty'];
                            $arr = [
                                'total_qty' => $product['item_qty'],
                                'unit_price' => $product['item_price'],
                                'total_price' => $productTotalPrice,
                                'sellable_qty' => $sellable,
                                'updated_at' => Carbon::now()
                            ];
                            //item stock and price update for decrease qty or price
                            $productData = ItemsModel::find($data->product_id);
                            $finalDataQty = ($productData->total_qty - $data->total_qty);
                            $ItemQtyTotal = $finalDataQty + ($product['item_qty']);
                            $finalDataPrice = $productData->total_price - ($data->total_qty * $data->unit_price);
                            $ItemPriceTotal = $finalDataPrice + ($product['item_price'] * $product['item_qty']);
                            //update entry details table
                            ProductsEntryDetailsModel::where('id', $data->id)->update($arr);
                            //update item table
                            $itemIdArr = [
                                'total_qty' => $ItemQtyTotal,
                                'total_price' => $ItemPriceTotal,
                                'updated_at' => Carbon::now(),
                            ];

                            $entryTotalPrice = $entryTotalPrice + $productTotalPrice;
                            ItemsModel::where('id', $data->product_id)->update($itemIdArr);
                        }

                    } elseif ($product['item_qty'] >= $data->total_qty) {
                        //product qty and price update for increase qty or price
                        $sellable = $data->sellable_qty + ($product['item_qty'] - $data->total_qty);
                        $productTotalPrice = $product['item_price'] * $product['item_qty'];
                        $arr = [
                            'total_qty' => $product['item_qty'],
                            'unit_price' => $product['item_price'],
                            'total_price' => $productTotalPrice,
                            'sellable_qty' => $sellable,
                            'updated_at' => Carbon::now()
                        ];
                        //item stock and price update for increase qty or price
                        $productData = ItemsModel::find($data->product_id);
                        $finalDataQty = ($productData->total_qty - $data->total_qty);
                        $ItemQtyTotal = $finalDataQty + ($product['item_qty']);
                        $finalDataPrice = $productData->total_price - ($data->total_qty * $data->unit_price);
                        $ItemPriceTotal = $finalDataPrice + ($product['item_price'] * $product['item_qty']);
                        //update entry details table
                        ProductsEntryDetailsModel::where('id', $data->id)->update($arr);
                        //update item table
                        $itemIdArr = [
                            'total_qty' => $ItemQtyTotal,
                            'total_price' => $ItemPriceTotal,
                            'updated_at' => Carbon::now(),
                        ];
                        $entryTotalPrice = $entryTotalPrice + $productTotalPrice;
                        ItemsModel::where('id', $data->product_id)->update($itemIdArr);
                    }
                }
                //update entry table
                $itemGrandArr = [
                    'total_price' => $entryTotalPrice,
                    'updated_at' => Carbon::now(),
                ];
                ProductsEntryModel::where('id', $data->stock_entry_id)->update($itemGrandArr);
            }
            DB::commit();
            Alert::toast('Data successfully updated', 'success')->width('375px');
        }catch (\Exception $e) {
            DB::rollback();
            Alert::toast('Data did not update successfully','error')->width('570px');
        }
        return redirect()->route('products.entry.index');

    }
}
