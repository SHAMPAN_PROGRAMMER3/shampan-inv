<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CrudModel;
use App\Models\JoinModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\Models\User;



class SuperAdminController extends Controller
{
    /*public function __construct($name,UserProvider $provider,Session $session,Request $request = null)

    {

        $this->name = $name;

        $this->session = $session;

        $this->request = $request;

        $this->provider = $provider;

    }*/

    public function adminRole()
    {
        $data['roles'] = DB::table('roles')->orderBy('id', 'desc')->paginate(20);
        return view('admin.roles.index', $data);
    }

    public function roleCreate()
    {
        return view('admin.roles.create');
    }

    public function saveRole(Request $request)
    {
        if ($request->input('roleId') != NULL) {
            $data['name'] = $request->input('name');
            $id = $request->input('roleId');
            DB::table('roles')->where('id', $id)->update($data);
        } else {
            $name = $request->input('name');
            $role = Role::create(['name' => $name]);
        }
        return Redirect::to('role')->with('message', 'Add new role successfully.');
    }

    public function roleEdit($id)
    {
        $role = DB::table('roles')->where('id', $id)->first();
        return view('admin.roles.create', compact('role'));
    }

    public function adminPermission()
    {
        $where = $orderBy = $groupBy = [];
        $data['permissions'] = CrudModel::findAll('permissions', ['parent_id' => 0]);
        $data['getchildperm'] = JoinModel::findChildPerm();
        $data['perm'] = CrudModel::findAll('permissions');
        $data['childperm'] = CrudModel::findAll('permissions');
        return view('admin.permissions.index', $data);
    }

    public function permissionCreate()
    {
        $data['getpermission'] = CrudModel::findAll('permissions',[],['parent_id'=>'asc']);
        return view('admin.permissions.create', $data);
    }

    public function permEdit($id)
    {
        $data['getperm'] = CrudModel::find('permissions', ['id' => $id]);
        $data['getpermission'] = CrudModel::findAll('permissions');
        return view('admin.permissions.create', $data);
    }

    public function childpermEdit($parent_id, $id)
    {
        $data['getperm'] = CrudModel::find('permissions', ['id' => $parent_id]);
        $data['getchild'] = CrudModel::find('permissions', ['id' => $id]);
        $data['getpermission'] = CrudModel::findAll('permissions');
        return view('admin.permissions.childperm', $data);
    }

    public function saveChildPerm(Request $request)
    {
        $name = $request->input('name');
        $child_id = $request->input('child_id');
        $data['parent_id'] = $child_id;
        $oldchildID = $request->input('oldchild_id');
        $newdata['parent_id'] = 0;
        $id = $request->input('permissionId');
        DB::table('permissions')->where('id', $id)->update($data);
        return Redirect::to('permission');
    }

    public function savePermission(Request $request)
    {
        if ($request->input('permissionId') != NULL) {
            $data['name'] = $request->input('name');
            $data['parent_id'] = $request->input('parent_id');
            $id = $request->input('permissionId');
            DB::table('permissions')->where('id', $id)->update($data);
        } else {
            $name = $request->input('name');
            $parent_id = $request->input('parent_id');
            $permission = Permission::create(['name' => $name, 'parent_id' => $parent_id]);
        }
        return Redirect::to('permission')->with('message', 'Add new permission successfully.');
    }
    public function permissionEdit($id)
    {
        $permission = DB::table('permissions')->where('id', $id)->first();
        return view('admin.permissions.create', compact('permission'));
    }
    public function permissionAssign($id)
    {
        $where = $orderBy = $groupBy = [];
        $permissionList = DB::table('permissions AS per')
            ->select("per.*", "rhp.permission_id")
            ->leftJoin('role_has_permissions AS rhp', function ($join) use ($id) {
                $join->on('rhp.permission_id', '=', 'per.id');
                $join->where('rhp.role_id', $id);
            })
            ->get();

        //dd($data['permissionList']);
        $data['role'] = DB::table('roles')->where('id', $id)->first();
        $cPermissions = [];

        foreach($permissionList as $key=>$val)
        {
            $nofArray = count($cPermissions);
            if($val->parent_id == 0)
            {
                $cPermissions[$nofArray] = $permissionList[$key];
                unset($permissionList[$key]);
                foreach($permissionList as $k=>$v)
                {
                    if($v->parent_id==$val->id)
                    {
                        $nofArray = count($cPermissions);
                        $cPermissions[$nofArray] = $permissionList[$k];
                        unset($permissionList[$k]);

                    }
                    else
                    {
                        if(isset($cPermissions) && !empty($cPermissions))
                        {
                            foreach($cPermissions as $ik=>$vv)
                            {
                                if($v->parent_id==$vv->id)
                                {
                                    if(isset($cPermissions[$ik+1]) && !empty($cPermissions[$ik+1]))
                                    {
                                        $nofArray = count($cPermissions);
                                        for($i=$ik+1;$i<=$nofArray;$i++)
                                        {
                                            $cPermissions[$i+1] = $cPermissions[$i];
                                        }

                                    }

                                    $cPermissions[$ik+1] = $permissionList[$k];
                                    unset($permissionList[$k]);
                                    continue 2;

                                }

                            }
                        }
                    }
                }
            }

        }
        //dd($cPermissions);

        $data['permissionList'] = $cPermissions;
        return view('admin.roles.assign', $data);
    }

    public function assignPermission(Request $request)
    {
        $role_id = $request->input('role_id');
        $role = Role::findById($role_id);
        $permission_ids = $request->input('permission_id');
        DB::table('role_has_permissions')->where('role_id', '=', $role_id)->delete();
        foreach ($permission_ids as $key => $id) {
            $role->givePermissionTo($id);
        }
        return Redirect::to('role')->with('message', 'Permission assign successfully.');
    }

    public function adminList()
    {
        $users = CrudModel::find('users',['id'=>auth()->user()->id]);
        $where['users.status'] = 1;
        /*if(isset(auth()->user()->company_id))
        {
            $where['users.company_id'] = auth()->user()->company_id;
        }*/
      //  $data['company'] = CrudModel::findAll('company', ['status' => 1]);
     //   $data['department'] = CrudModel::findAll('department', ['status' => 1]);
     //   $data['designation'] = CrudModel::findAll('designation_info', ['status' => 1]);
        $data['adminRole'] = CrudModel::findAll('roles')->all();
        //dd($data);

        $data['paginateData'] = DB::table('users')
            ->select('users.*',
               // 'hrm_employee_info.emp_code',
            //    'company.company_name as company_name',
            //    'department.dep_name',
           //     'designation_info.desi_name',
                'roles.name as role_name'
            )
            //->leftJoin('hrm_employee_info', 'hrm_employee_info.id', '=', 'users.emp_id')
            ->leftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
            ->leftJoin('roles', 'roles.id', '=', 'model_has_roles.role_id')
           // ->leftJoin('company', 'company.id', '=', 'users.company_id')
           // ->leftJoin('department', 'department.id', '=', 'users.dep_id')
           // ->leftJoin('designation_info', 'designation_info.id', '=', 'users.desi_id')
            ->orderBy('users.id', 'desc')
            ->where($where)
            ->get();

        //dd($data['paginateData']);
        return view('admin.roles.adminList', $data);
    }
    public function adminListFilter(Request $request)
    {
        //$where = $orderBy = $groupBy = [];
        $users = CrudModel::find('users',['id'=>auth()->user()->id]);
            $where[] = ['company.status', '=',1];
            $data['company'] = CrudModel::findAll('company', ['status' => 1]);
            $where = ['users.status' => 1,'hrm_employee_info.is_regined' => 0];
            $data['adminRole'] = CrudModel::findAll('roles')->all();
            //fetch department
            $depWhere['status']=1;
            if(isset($request->group_id))
            {
                $depWhere['group_id']=$request->group_id;
            }
            $data['department'] = CrudModel::findAll('department',$depWhere);
            //fetch department
            $desiWhere['status']=1;
            if(isset($request->group_id))
            {
                $desiWhere['group_id']=$request->group_id;
            }
            if(isset($request->dep_id))
            {
                $desiWhere['dep_id']=$request->dep_id;
            }
            $data['designation'] = CrudModel::findAll('designation_info', $desiWhere);

        if(isset($request->group_id) && !empty($request->group_id))
        {
            $where[] = ['designation_info.group_id', '=',$request->group_id];
            $data['group_id'] = $request->group_id;
        }
        if(isset($request->dep_id) && !empty($request->dep_id))
        {
            $where[] = ['designation_info.dep_id', '=',$request->dep_id];
            $data['dep_id'] = $request->dep_id;
        }
        if (isset($request->desi_id) && !empty($request->desi_id)) {
            $where[] = ['designation_info.id', '=', $request->desi_id];
            $data['desi_id'] = $request->desi_id;
        }
        if (isset($request->role_id) && !empty($request->role_id)) {
            $where[] = ['roles.id', '=', $request->role_id];
            $data['role_id'] = $request->role_id;
        }
        //dd($data);
        $where[] = ['designation_info.status', '=',1];

        $data['paginateData'] = DB::table('users')
            ->select('users.*',
                'hrm_employee_info.emp_code',
                'company.group_name as company_name',
                'department.dep_name',
                'designation_info.desi_name',
                'roles.name as role_name',
            )
            ->leftJoin('hrm_employee_info', 'hrm_employee_info.id', '=', 'users.emp_id')
            ->leftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'hrm_employee_info.id')
            ->leftJoin('roles', 'roles.id', '=', 'model_has_roles.role_id')
            ->leftJoin('company', 'company.id', '=', 'hrm_employee_info.group_id')
            ->leftJoin('department', 'department.id', '=', 'hrm_employee_info.dep_id')
            ->leftJoin('designation_info', 'designation_info.id', '=', 'hrm_employee_info.desi_id')
            ->orderBy('users.id', 'desc')
            ->where($where)
            ->get();
        return view('admin.roles.adminList', $data);
    }
    public function adminCreate()
    {
        return view('admin.roles.adminCreate');
    }

    public function saveAdmin(Request $request)
    {
        if ($request->input('adminId') != NULL) {
            $data['name'] = $request->input('name');
            $data['email'] = $request->input('email');
            $data['password'] = Hash::make($request->input('password'));
            $id = $request->input('adminId');
            DB::table('users')->where('id', $id)->update($data);
        }
        else {
            $name = $request->input('name');
            $email = $request->input('email');
            $password = Hash::make($request->input('password'));
            $branch_id = $request->input('branch_id');
            $admin = User::create(['name' => $name, 'email' => $email, 'password' => $password, 'branch_id' => $branch_id]);
        }
        return Redirect::to('admin/list')->with('message', 'Add new admin successfully.');
    }

    public function adminEdit($id)
    {
        $admin = DB::table('users')->where('id', $id)->first();
        return view('admin.roles.adminCreate', compact('admin'));
    }

    public function roleAssign($id)
    {
        //dd(auth()->user()->company_id);
        $where=[];
        if(isset(auth()->user()->company_id))
        {
            $where['roles.status_slug']=1;
        }
        $roles = DB::table('roles')
            ->select("roles.*", "mhp.model_id")
            ->leftJoin('model_has_roles AS mhp', function ($join) use ($id) {
                $join->on('mhp.role_id', '=', 'roles.id');
                $join->where('mhp.model_id', $id);

            })
            ->where($where)
            ->get();
        //dd($roles);
        $admin = DB::table('users')->where('id', $id)->first();
        return view('admin.roles.roleassign', compact('roles', 'admin'));
    }

    public function assignRole(Request $request)
    {
        $role_id = $request->input('roleId');
        $role = Role::findById($role_id);
        $adminId = $request->input('adminId');
        $user = User::findOrFail($adminId);
        DB::table('model_has_roles')->where(['model_id' => $adminId])->delete();
        $user->assignRole($role);
        return back()->with('message', 'Role assign successfully.');
    }

   /* public function adminlogreport(Request $request)
    {
        //dd($request->all());
        //Filter
        $where[] = ['company.status', '=',1];
        $data['company'] = CrudModel::findAll('company', $where);
        //date filter

        if(!empty($request->all())) {
            if ($request->get('log_date_from')) {
                $where[] = ['user_log.created_at', '>=', $request->get('log_date_from')];
            }
            if ($request->get('log_date_to')) {
                $where[] = ['user_log.created_at', '<=', $request->get('log_date_to')];
            }
        }
        $data['request'] = $request->all();
        //fetch department
        $depWhere['status']=1;
        if(isset($request->group_id))
        {
            $depWhere['group_id']=$request->group_id;
        }
        $data['department'] = CrudModel::findAll('department',$depWhere);
        //fetch designation
        $desiWhere['status']=1;
        if(isset($request->group_id))
        {
            $desiWhere['group_id']=$request->group_id;
        }
        if(isset($request->dep_id))
        {
            $desiWhere['dep_id']=$request->dep_id;
        }
        $data['designation'] = CrudModel::findAll('designation_info', $desiWhere);
        //fetch employee
        $empWhere['status']=1;
        if(isset($request->group_id))
        {
            $empWhere['group_id']=$request->group_id;
        }
        if(isset($request->dep_id))
        {
            $empWhere['dep_id']=$request->dep_id;
        }
        if(isset($request->desi_id))
        {
            $empWhere['desi_id']=$request->desi_id;
        }
        $data['allEmployee'] = CrudModel::findAll('hrm_employee_info', $empWhere);

        //dd();
        if(isset($request->group_id) && !empty($request->group_id))
        {
            $where[] = ['designation_info.group_id', '=',$request->group_id];
            $data['group_id'] = $request->group_id;
        }
        if(isset($request->dep_id) && !empty($request->dep_id))
        {
            $where[] = ['designation_info.dep_id', '=',$request->dep_id];
            $data['dep_id'] = $request->dep_id;
        }
        if (isset($request->desi_id) && !empty($request->desi_id)) {
            $where[] = ['designation_info.id', '=', $request->desi_id];
            $data['desi_id'] = $request->desi_id;
        }
        if (isset($request->emp_id) && !empty($request->emp_id)) {
            $where[] = ['hrm_employee_info.id', '=', $request->emp_id];
            $data['emp_id'] = $request->emp_id;
        }

        $data['logreport'] = DB::table('user_log')
            ->select('user_log.*',
                'hrm_employee_info.emp_name',
                'hrm_employee_info.middle_name',
                'hrm_employee_info.last_name',
                'company.group_name',
                'department.dep_name',
                'designation_info.desi_name',
            )
            ->where($where)
            ->leftJoin('users','users.id','=','user_log.user_id')
            ->leftJoin('hrm_employee_info','hrm_employee_info.id','=','users.emp_id')
            ->leftJoin('company','company.id','=','hrm_employee_info.group_id')
            ->leftJoin('department','department.id','=','hrm_employee_info.dep_id')
            ->leftJoin('designation_info','designation_info.id','=','hrm_employee_info.desi_id')
            ->orderBy('user_log.id', 'desc');
        $data['report'] = $data['logreport']->get();
        //dd($data);
        return view('admin.userlog.index', $data);
    }*/
}
