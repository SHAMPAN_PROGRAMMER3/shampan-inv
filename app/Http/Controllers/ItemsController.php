<?php

namespace App\Http\Controllers;

use App\Models\ItemsModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\CrudModel;
use App\Models\JoinModel;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;
use File;
use DB;


class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $where[]=['items.status','=',1];
        $arr=[];
        if(!empty($request->all())) {
            if ($request->get('item_code')) {
                $arr[] = ['items.item_code', '=', $request->get('item_code')];
            }
        }
       // dd(JoinModel::findAllfilterItems($arr));
        $data['items'] = JoinModel::findAllfilterItems($arr,[],['qty'=>'desc','item_name'=>'asc']);
        $data['requests']= $request->all();
        //dd($data);
        return view('items.index', $data);
    }

    public function create(Request $request)
    {
        $data['item'] = ItemsModel::orderBy('id','desc')->first();
        return view('items.create',$data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $rules = [
            'item_name' => 'required|max:255'
        ];
        $message = [
            'item_name.required' => 'Item Name is required.'
        ];
        if(isset($request->item_image))
        {
            $rules['item_image']= 'mimes:jpeg,jpg,png,gif';
            $message['item_image.mimes']= 'Item Image type is invalid';
        }
        $this->validate($request, $rules, $message);

        $data = $request->except('_token','item_image');
        $data['created_at'] = date('Y-m-d');
        $data['status'] = 1;

        /*save image*/
        if (isset($request->item_image) && !empty($request->item_image)) {
            $image = time() . Str::random(7) . '.' . $request->item_image->extension();
            $request->item_image->move(public_path('images/items/'), $image);
            $data['item_image'] = $image;
        }
        /*End*/

            $insertedId = CrudModel::save('items', $data);
            Alert::toast('Item created successfully', 'success')->width('375px');

        return redirect()->route('items.index');
    }
    public function categoryValidation($data)
    {
        $rules = [
            'category_name'  => 'required',
            'parent_id'  => 'required',
        ];
        $messages = [
            'category_name.required' => 'Category name is required.',
            'parent_id.required' => 'Parent category name is required.',
        ];
        return validator($data, $rules, $messages);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $emp = CrudModel::find('users',['id'=>auth()->user()->id]);
        $data['item'] = ItemsModel::where('status',1)->find($id);
        return view('items.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'item_name' => 'required|max:255',
            'item_code' => 'required'
        ];
        $message = [
            'item_name.required' => 'Item Name is required.',
            'item_code.required' => 'Item Code is required.'
        ];

        if(isset($request->item_image))
        {
            $rules['item_image']= 'mimes:jpeg,jpg,png,gif';
            $message['item_image.mimes']= 'Item Image type is invalid';
        }
        $this->validate($request, $rules, $message);

        $data = $request->except('_token','item_image', 'item_id');
        /*save image*/
        if (isset($request->item_image) && !empty($request->item_image)) {
            $image = time() . Str::random(7) . '.' . $request->item_image->extension();
            $request->item_image->move(public_path('images/items/'), $image);
            $data['item_image'] = $image;
            $item = ItemsModel::find($request->get('item_id'));
            //Delete file
            if (isset($item->item_image)){
                $image_path = "images/items/" . $item->item_image;  // Value is not URL but directory file path
                if (\Illuminate\Support\Facades\File::exists($image_path)) {
                    File::delete($image_path);
                }
            }
        }
        /*End*/
        $updated_at= Carbon::now();
        $data['updated_at'] = $updated_at;
        $data['status'] = 1;
        //dd($data);
        CrudModel::update('items', $data, ['id'=>$request->get('item_id')]);
        Alert::toast('Data successfully Updated','success')->width('375px');

        return redirect()->route('items.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $data = ItemsModel::find($id);
        $updateData = ItemsModel::where('id',$id)->update(['status'=>0]);
        if($updateData)
        {
            Alert::toast('Data Deleted successfully.','success')->width('375px');
            return response()->json(['status' => 1]);
        }
        else{
            return response()->json(['status' => 0]);
        }
    }
}
