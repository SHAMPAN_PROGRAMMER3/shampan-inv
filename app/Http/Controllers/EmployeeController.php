<?php

namespace App\Http\Controllers;

use App\Models\CrudModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use Carbon\Carbon;

class EmployeeController extends Controller
{
    public function index()
    {
        $data['users'] = CrudModel::findAll('users', ['status' => 1]);
        return view('employee.index', $data);
    }

    public function create()
    {
        return view('employee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'required|min:6'
        ];

        $message = [
            'required' => 'The :attribute field is required.'
        ];

        $this->validate($request, $rules, $message);

        // data array make for save
        $data = $request->only(
            'name',
            'email',
        );

        $created_at = Carbon::now();
        $data['created_at'] = $created_at;
        $data['password'] = Hash::make($request->get('password'));

        //dd($data);
        CrudModel::save('users',$data);
        Alert::toast('Data successfully Inserted', 'success')->width('375px');
        return redirect()->route('employee.index');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['users'] = User::find($id);
        //dd($data);
        return view('employee.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //dd($id);
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required',
        ];

        $message = [
            'required' => 'The :attribute field is required.'
        ];
        $this->validate($request, $rules, $message);

        // data array make for save
        $data = $request->only(
            'name',
            'email',
        );

        $updated_at = Carbon::now();
        $data['updated_at'] = $updated_at;
        CrudModel::update('users', $data, ['id' => $request->emp_id]);
        //dd($data);
        Alert::toast('Data successfully Updated', 'success')->width('375px');
        return redirect()->route('employee.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = User::find($id);
        $updateData = User::where('id',$id)->update(['status'=>0]);
        if($updateData)
        {
            Alert::toast('Data Deleted successfully.','success')->width('375px');
            return response()->json(['status' => 1]);
        }
        else{
            return response()->json(['status' => 0]);
        }
    }
    public function changePassword(Request $request)
    {
        $postData = $request->except('_token');
        /*validation*/
        if($postData['old_password'] == '' || $postData['new_password'] == '' || $postData['confirm_password'] == '') {
            Alert::toast('All fields are required.','error')->width('375px');
            return redirect()->route('employee.index');
        }
        else{
            /*change password*/
            $userId = $request->get('user_id');
            $user = User::find($userId);
            if (Hash::check($postData['old_password'], $user->password)) {
                if($postData['new_password'] == $postData['confirm_password']){
                    $user->password = Hash::make($postData['new_password']);
                    $user->save();
                    Alert::toast('Your password changed successfully.','success')->width('375px');
                    return redirect()->route('employee.index');
                }
                else{
                    Alert::toast('New password and Confirm password do not match.','error')->width('375px');
                    return redirect()->route('employee.index');
                }
            }
            else{
                Alert::toast('Your old password does not match.','error')->width('375px');
                return redirect()->route('employee.index');
            }
        }

    }
}
