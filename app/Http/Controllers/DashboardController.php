<?php

namespace App\Http\Controllers;

use App\Models\CompaniesModel;
use App\Models\NoticesModel;
use App\Models\CrudModel;
use App\Models\JoinModel;
use App\Models\EmployeeLeavesModel;
use DB;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $data = [];
        $emp = JoinModel::findEmployee(['users.id'=>auth()->user()->id]) ;
        //dd($emp);


        /*if($emp->groupid != null)
        {
            $totalEmployee = CrudModel::findAllSum('hrm_employee_info',['status' => 1, 'is_regined' => 0, 'group_id'=>$emp->groupid],$orderBy = [])->select(DB::raw('sum(status) as total_employee'))->first();
            $totalPresent = CrudModel::findAllSum('hrm_attendance',['status' => 1, 'group_id'=>$emp->group_id, 'att_date' => date('Y-m-d')],$orderBy = [])->select(DB::raw('sum(status) as total_present'))->first();

            $empLeaveModel = new EmployeeLeavesModel();
            $leaves = $empLeaveModel->getData($emp->group_id);

            isset($totalEmployee)?$data['totalEmployee'] = $totalEmployee->total_employee : $data['totalEmployee'] = 0;
            isset($totalPresent)?$data['totalPresent'] = $totalPresent->total_present : $data['totalPresent'] = 0;
            isset($leaves)?$data['totalLeave'] = count($leaves) : $data['totalLeave'] = 0;

            $noticeModel = new NoticesModel();
            $data['notice'] = $noticeModel->getData($emp->groupid);
           //dd($data);

        }
        else
        {

                $data['company'] = CompaniesModel::where(['status' => 1])->get();
                if(isset($data['company']) && !empty($data['company']))
                {
                    foreach ($data['company'] as $key=>$company)
                    {
                        $totalEmployee = CrudModel::findAllSum('hrm_employee_info',['status' => 1, 'is_regined' => 0,'group_id'=>$company->id], $orderBy = [])->select(DB::raw('sum(status) as total_employee'))->first();
                        $totalPresent = CrudModel::findAllSum('hrm_attendance',['status' => 1,'group_id'=>$company->id, 'att_date' => date('Y-m-d')],$orderBy = [])->select(DB::raw('sum(status) as total_present'))->first();

                        isset($totalEmployee)?$data['data']['totalEmployee'][$company->group_name] = $totalEmployee->total_employee : $data['data']['totalEmployee'][$company->group_name] = 0;
                        isset($totalPresent)?$data['data']['totalPresent'][$company->group_name] = $totalPresent->total_present : $data['data']['totalPresent'][$company->group_name] = 0;
                        isset($leaves)?$data['data']['totalLeave'][$company->group_name] = count($leaves) : $data['data']['totalLeave'][$company->group_name] = 0;
                        $data['data']['totalAbsent'][$company->group_name] = $data['data']['totalEmployee'][$company->group_name] - $data['data']['totalPresent'][$company->group_name] - $data['data']['totalLeave'][$company->group_name];

                        $noticeModel = new NoticesModel();
                        $data['notice'] = $noticeModel->getData($emp->group_id);
                    }

                }

                //dd($data['data']);
        }*/
        //dd(auth()->user());
        return view('dashboard',$data);
    }

    public function monitor()
    {

        return view('monitor');
    }

    public function iframe()
    {
        $data['url'] = 'https://www.shampan.com';


        //dd($data['pageContent']);
        return view('iframe',$data);
    }
}
