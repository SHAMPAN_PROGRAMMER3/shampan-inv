<?php

namespace App\Http\Controllers;

use App\Models\ItemsModel;
use App\Models\ProductOutDetailsModel;
use App\Models\ProductOutHistoryModel;
use App\Models\ProductOutModel;
use App\Models\ProductsEntryDetailsModel;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class ProductOutController extends Controller
{
    public function index()
    {
        $data['productOutList'] = DB::table('stock_out_entry')
            ->leftjoin('users','stock_out_entry.out_by','=','users.id')
            ->where('stock_out_entry.status','=',1)
            ->select('stock_out_entry.*','users.name as emp_name')
            ->orderBy('stock_out_entry.created_at','desc')
            ->get();
        return view('products-out.index', $data);
    }
    public function create()
    {
        return view('products-out.create');
    }
    public function getProduct(Request $request)
    {
        $where['items.item_code'] = $request->get('item_code');
        $where['items.status'] = 1;
        $where[] = ['items.total_qty','>',0];
        $data = DB::table('items')
            ->where($where)
            ->select('items.*')
            ->get();
        return $data;
    }
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            //validation
            $arrProduct = [];
            foreach ($request->get('products') as $key=>$value)
            {
                if($value['qty']>0)
                {
                    $arrProduct[$key] = $value;
                }
            }
            if(!empty($arrProduct)) {
                /*data save in stock_out_entry table*/

                $productOutModel = new ProductOutModel();
                $productOutModel->out_by = auth()->user()->id;
                $productOutModel->product_type_qty = count($arrProduct);
                $productOutModel->serial_no = strtotime(date('Y-m-d H:i:s'));
                $productOutModel->out_date = date('Y-m-d');
                $productOutModel->status = 1;
                $productOutModel->created_at = Carbon::now();
                $productOutModel->save();
                $productOutId = $productOutModel->id;

                $arr = [];
                $totalPrice = 0;
                foreach ($arrProduct as $key => $product) {
                    /*Stock update*/
                    $outQty = intval($product['qty']);
                    $productTotalPrice = 0;
                    $productData = DB::table('stock_entry_details')
                        ->select('stock_entry_details.*')
                        ->where('stock_entry_details.product_id', $product['item_id'])
                        ->where([['sellable_qty', '>', 0]])
                        ->orderBy('created_at', 'asc')
                        ->get();
                    $history = [];
                    foreach ($productData as $value) {
                        if ($outQty >= $value->sellable_qty && $outQty != 0) {
                            ProductsEntryDetailsModel::where('id', $value->id)->update(['sellable_qty' => 0]);
                            $outQty = $outQty - $value->sellable_qty;

                            //$history[] = [$value->sellable_qty => $value->unit_price];
                            $history[] = [
                                'id'=>$value->id,
                                'qty'=>$value->sellable_qty,
                                'unit_price'=> $value->unit_price
                            ];
                            $productTotalPrice = $productTotalPrice + ($value->sellable_qty * $value->unit_price);
                        } elseif ($outQty < $value->sellable_qty && $outQty != 0) {
                            ProductsEntryDetailsModel::where('id', $value->id)->update(['sellable_qty' => (intval($value->sellable_qty) - $outQty)]);
                            //$history[] = [$outQty => $value->unit_price];
                            $history[] = [
                                'id'=>$value->id,
                                'qty'=>$outQty,
                                'unit_price'=> $value->unit_price
                            ];
                            $productTotalPrice = $productTotalPrice + ($outQty * $value->unit_price);

                            $outQty = 0;
                        }

                    }
                    /*data save in stock_out_details table*/
                    $productOutDetailsModel = new ProductOutDetailsModel();
                    $productOutDetailsModel->stock_out_id = $productOutId;
                    $productOutDetailsModel->product_id = $product['item_id'];
                    $productOutDetailsModel->created_at = Carbon::now();
                    $productOutDetailsModel->item_name = $product['item_name'];
                    $productOutDetailsModel->item_code = $product['item_code'];
                    $productOutDetailsModel->product_qty = $product['qty'];
                    $productOutDetailsModel->total_price = $productTotalPrice;
                    $productOutDetailsModel->qty_price_history = json_encode($history);
                    $productOutDetailsModel->save();
                    $productOutDetailsId = $productOutDetailsModel->id;

                    /*data save in product history*/
                    foreach ($history as $key2 => $value2)
                    {
                        $arr[$key2]['out_details_id'] = $productOutDetailsId;
                        $arr[$key2]['entry_details_id'] = $value2['id'];
                        $arr[$key2]['product_qty'] = $value2['qty'];
                        $arr[$key2]['returnable_qty'] = $value2['qty'];
                        $arr[$key2]['unit_price'] = $value2['unit_price'];
                        $arr[$key2]['total_price'] = $value2['qty']*$value2['unit_price'];
                        $arr[$key2]['created_at'] = Carbon::now();
                    }

                    if (count($arr) > 0) {
                        ProductOutHistoryModel::insert($arr);
                    }
                    $totalPrice = $totalPrice + $productTotalPrice;

                    //Product total stock update
                    $item = ItemsModel::find($product['item_id']);
                    ItemsModel::where('id', $product['item_id'])->update(['total_qty' => ($item->total_qty - $product['qty']), 'total_price' => ($item->total_price - $productTotalPrice)]);

                }
                //update stock out total price data;
                ProductOutModel::where('id', $productOutId)->update(['total_price' => $totalPrice]);
                DB::commit();
                Alert::toast('Data successfully Inserted', 'success')->width('375px');
            }else{
                Alert::toast('Please give valid quantity of products','error')->width('570px');
            }
        }catch (\Exception $e) {
            DB::rollback();
            Alert::toast('Data did not Insert successfully','error')->width('570px');
        }

        return redirect()->route('products.out.index');
    }

    public function show($id)
    {
        $where['stock_out_entry.id'] = $id ;
        $data['productOutDetails'] = ProductOutModel::where($where)
            ->leftjoin('stock_out_details','stock_out_entry.id','=','stock_out_details.stock_out_id')
            ->leftjoin('items','stock_out_details.product_id','=','items.id')
            ->select('stock_out_details.*', 'items.item_name', 'items.item_code', 'stock_out_entry.total_price as grand_total')
            ->get();
        return view('products-out.view', $data);
    }
}
