<?php

namespace App\Http\Controllers;

use App\Models\CrudModel;
use App\Models\UnitsModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class UnitController extends Controller
{
    //
    public function index()
    {
        $where = [];
        $users = CrudModel::find('users',['id'=>auth()->user()->id]);

        if(isset($users->company_id) && !empty($users->company_id))
        {
            //dd($users->company_id);
            $where = ['unit.status'=>1,'unit.company_id'=>$users->company_id];
            $data['company_id'] = $users->company_id;
            $data['units'] = DB::table('unit')
                ->leftjoin('company','unit.company_id','=','company.id')
                ->where($where)
                ->select('unit.*','company.company_name')
                ->get();
        }
        else
        {
            $whereDep = ['unit.status'=>1];
            $data['units'] =  DB::table('unit')
                ->leftjoin('company','unit.company_id','=','company.id')
                ->where($whereDep)
                ->select('unit.*','company.company_name')
                ->get();
        }
        //dd($data);
        return view('settings.unit.index',$data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = CrudModel::find('users',['id'=>auth()->user()->id]);
        if(!isset($users->company_id) && empty($users->company_id))
        {
            $where = ['company.status'=>1];
            $data['companies'] = CrudModel::findAll('company', $where);
        }
        else
        {
            $data['company_id'] = $users->company_id;
        }
        return view('settings.unit.create',$data);

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'unit_name' => 'required|max:255',
            'short_name' => 'required|max:255',
            'company_id' => 'required|max:255',
        ];
        $message = [
            'unit_name.required' => 'Unit Name field is required.',
            'short_name.required' => 'Short Name field is required.',
            'company_id.required' => 'Company Name field is required.',
        ];
        $this->validate($request, $rules, $message);
        $data = $request->only('unit_name', 'short_name', 'company_id');

        $created_at= Carbon::now();
        $data['created_at'] = $created_at;
        CrudModel::save('unit', $data);
        Alert::toast('Data successfully Inserted','success')->width('375px');

        return redirect()->route('unit.index');
    }
    public function edit($id)
    {
        $users = CrudModel::find('users',['id'=>auth()->user()->id]);
        if(!isset($users->company_id) && empty($users->company_id))
        {
            $where = ['company.status'=>1];
            $data['companies'] = CrudModel::findAll('company', $where);
        }
        else
        {
            $data['company_id'] = $users->company_id;
        }
        $data['unit'] = UnitsModel::find($id);
        //dd($data);
        return view('settings.unit.edit',$data);
    }

    public function update(Request $request){

        $rules = [
            'unit_name' => 'required|max:255',
            'short_name' => 'required|max:255',
            'company_id' => 'required|max:255',
        ];
        $message = [
            'unit_name.required' => 'Unit Name field is required.',
            'short_name.required' => 'Short Name field is required.',
            'company_id.required' => 'Company Name field is required.',
        ];
        $this->validate($request, $rules, $message);
        $data = $request->only('unit_name', 'short_name', 'company_id');

        $updated_at= Carbon::now();
        $data['updated_at'] = $updated_at;

        CrudModel::update('unit', $data, ['id'=>$request->get('unit_id')]);
        Alert::toast('Data successfully Updated','success')->width('375px');
        return redirect()->route('unit.index');
    }

    public function delete($id)
    {
        $data = UnitsModel::find($id);
        $updateData = UnitsModel::where('id',$id)->update(['status'=>0]);
        if($updateData)
        {
            Alert::toast('Data Deleted successfully.','success')->width('375px');
            return response()->json(['status' => 1]);
        }
        else{
            return response()->json(['status' => 0]);
        }
    }
}
