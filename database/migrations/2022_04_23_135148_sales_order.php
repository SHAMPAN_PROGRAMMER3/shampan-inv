<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SalesOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_order', function (Blueprint $table) {
            $table->id();
            $table->integer('warehouse_id');
            $table->integer('customer_id');
            $table->integer('entry_by');
            $table->string('sub_total');
            $table->string('discount')->nullable();
            $table->string('point_discount')->nullable();
            $table->string('vat')->nullable();
            $table->string('grand_total');
            $table->string('point_receive')->nullable();
            $table->string('point_use')->nullable();
            $table->enum('payment_status', ['paid', 'unpaid']);
            $table->string('payment_type')->nullable()->comment('1= cash,2= bkash,3= nagad,4= card');
            $table->string('transaction_id')->nullable();
            $table->tinyInteger('status')->default('1')->comment('0 = inactive,1= active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_order');
    }
}
