<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductDamageDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_damage_details', function (Blueprint $table) {
            $table->id();
            $table->integer('product_damage_id');
            $table->integer('product_id');
            $table->integer('product_qty');
            $table->double('total_price');
            $table->string('qty_price_history')->nullable();
            $table->string('item_name');
            $table->string('item_code');
            $table->tinyInteger('status')->default('1')->comment('0 = inactive,1= active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_damage_details');
    }
}
