<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StockEntryDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_entry_details', function (Blueprint $table) {
            $table->id();
            $table->integer('stock_entry_id');
            $table->integer('product_id');
            $table->integer('sellable_qty');
            $table->integer('total_qty');
            $table->double('unit_price');
            $table->double('total_price');
            $table->tinyInteger('status')->default('1')->comment('0 = inactive,1= active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_entry_details');
    }
}
