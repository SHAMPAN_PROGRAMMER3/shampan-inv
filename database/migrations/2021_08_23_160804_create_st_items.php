<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->char('item_name');
            $table->string('item_image')->nullable();
            $table->integer('unit_id')->nullable();
            $table->string('item_code')->nullable();
            $table->integer('total_qty')->nullable();
            $table->double('total_price')->nullable();
            $table->tinyInteger('status')->default('1')->comment('0 = inactive,1= active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
