<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductReturnDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_return_details', function (Blueprint $table) {
            $table->id();
            $table->integer('product_return_id');
            $table->integer('product_id');
            $table->integer('product_qty');
            $table->double('total_price');
            $table->string('qty_price_history')->nullable();
            $table->string('item_name');
            $table->string('item_code');
            $table->tinyInteger('status')->default('1')->comment('0 = inactive,1= active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_return_details');
    }
}
