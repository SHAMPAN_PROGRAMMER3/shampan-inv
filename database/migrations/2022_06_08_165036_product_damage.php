<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductDamage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_damage', function (Blueprint $table) {
            $table->id();
            $table->string('serial_no');
            $table->integer('damage_by');
            $table->string('damage_date');
            $table->integer('product_type_qty');
            $table->integer('entry_in_out_id');
            $table->double('total_price')->nullable();
            $table->enum('slug', ['out', 'entry']);
            $table->tinyInteger('status')->default('1')->comment('0 = inactive,1= active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_damage');
    }
}
