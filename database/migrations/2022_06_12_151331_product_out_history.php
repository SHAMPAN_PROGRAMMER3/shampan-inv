<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductOutHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_out_history', function (Blueprint $table) {
            $table->id();
            $table->integer('out_details_id');
            $table->integer('entry_details_id');
            $table->integer('product_qty');
            $table->double('unit_price');
            $table->double('total_price');
            $table->integer('returnable_qty');
            $table->tinyInteger('status')->default('1')->comment('0 = inactive,1= active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_out_history');
    }
}
